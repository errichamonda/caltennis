<?php

class Login {

    /**
     * Costruttore
     */
    public function __construct() {
        include_once "../../includes/database.php";
        setlocale(LC_TIME, 'ita', 'it_IT');
        $conn = wrap_db_connect();
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
    }

    function getLogin($username, $password) {
        $query = "SELECT username, password, salt "
                . "FROM users "
                . "WHERE username = '$username' "
                . "OR email = '$username'";
        $res = wrap_db_query($query);
        $result = wrap_db_fetch_array($res);
        $login_ok = false;
        if ($result) {
            // Usando la password inserita nella form e il salt (chiave) messa nel database, 
            // si controlla se la password inserita è identica alla password presente nel database
            $check_password = hash('sha256', $password . $result['salt']);
            for ($round = 0; $round < 65536; $round++) {
                $check_password = hash('sha256', $check_password . $result['salt']);
            }

            if ($check_password === $result['password']) {
                // Se il controllo va a buon fine si da ok
                $login_ok = true;
            }
            if ($login_ok) {
                // Si mettono i valori nelle variabili di $SESSION ovviamente togliendo
                // la chiave di cifratura e la password.
                unset($result['salt']);
                unset($result['password']);

                // Inserisco i dati dentro a user così da poter controllare dopo
                // nelle altre pagine se l'utente è loggato oppure no
                session_start();
                $_SESSION['user'] = $result['username'];
                // Reindirizza l'utente ad un'altra pagina 
                return "allow";
            } else {
                // Ti printa che il login è fallito 
                return "<span class='unselectable'>Login fallito</span>";
            }
        } else {
            return "<span class='unselectable'>Login fallito</span>";
        }
    }
}
