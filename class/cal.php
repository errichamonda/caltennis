<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cal
 *
 * @author strez
 */
class cal {

    private $dayCalendar;
    private $max;
    private $min;
    private $periodi;
    private $priv;
    private $tariffaid;
    private $campo;

    /**
     * Costruttore
     */
    public function __construct() {
        include "../includes/database.php";
        include "../includes/declares.php";
        setlocale(LC_TIME, 'ita', 'it_IT');
        $conn = wrap_db_connect();
        include "attivita.php";
        $this->attivita = new Attivita($conn);
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $this->setCampo();
        $this->setDayCalendar();
        $this->getMaxMinOra();
        $this->setPeriodi();
        $this->setPrivilegio();
    }

    /*
     * GET E SET
     */

    private function setPrivilegio() {
        if (isset($_SESSION['user'])) {
            $query = "SELECT tid FROM anagrafica, users WHERE username = '" . $_SESSION['user'] . "' AND anagrafica.uid = users.uid";
            $res = wrap_db_query($query);
            $result = wrap_db_fetch_array($res);
            $this->priv = $result['tid'];
        } else {
            $this->priv = "guest";
        }
    }

    private function getTariffa($id) {
        $db_query = "SELECT * FROM tariffe_$id";
        $res = wrap_db_query($db_query);
        $ret = mysqli_fetch_all($res, MYSQLI_ASSOC);
        return $ret;
    }

    private function setCampo() {
        $db_query = "SELECT * FROM campo WHERE cid = " . $_REQUEST['campo'];
        $res = wrap_db_query($db_query);
        $this->campo = mysqli_fetch_assoc($res);
    }

    private function getColoreTariffa($id) {
        $db_query = "SELECT colore FROM tariffe WHERE id = $id";
        $result = wrap_db_query($db_query);
        if (mysqli_num_rows($result) > 0) {
            $a = mysqli_fetch_assoc($result);
            return $a['colore'];
        } else {
            return false;
        }
    }

    private function setDayCalendar() {
        $query = "SELECT valore FROM impostazioni WHERE chiave = 'giorninelcalendario'";
        $res = wrap_db_query($query);
        $this->dayCalendar = mysqli_fetch_row($res)[0];
    }

    private function getMaxMinOra() {
        $count = 0;
        $db_query = "SELECT CONCAT('SELECT MAX(ora) AS max, MIN(ora) AS min FROM ', table_name, ' WHERE `0` IS NOT NULL OR `1` IS NOT NULL"
                . " OR `2` IS NOT NULL OR `3` != NULL OR `4` IS NOT NULL OR `5` IS NOT NULL OR `6` IS NOT NULL ')"
                . " AS statement FROM information_schema.tables WHERE table_name LIKE 'cal_camp" . $this->campo['cid'] . "%' GROUP BY table_name";
        $result = wrap_db_query($db_query);
        $p = mysqli_fetch_all($result);
        $query = "SELECT MIN(min) as mins, MAX(max) as maxs FROM ( ";
        foreach ($p as $x) {
            //return $x[0];
            $query .= $x[0];
            if ($count != count($p) - 1) {
                $query .= "\n UNION \n";
            } else {
                $query .= " ) AS temp";
            }
            $count++;
        }
        $res = wrap_db_query($query);
        $dud = mysqli_fetch_assoc($res);
        $this->max = $dud["maxs"];
        $this->min = $dud["mins"];
    }

    private function setPeriodi() {
        $db_query = "SELECT * FROM periodi ORDER BY inizio_tariffa ASC";
        $res = wrap_db_query($db_query);
        $this->periodi = mysqli_fetch_all($res, MYSQLI_ASSOC);
    }

    function setCalendario($id, $periodo) {
        $db_query = "SELECT * FROM cal_camp$id" . "_pe$periodo";
        $res = wrap_db_query($db_query);
        $ret = mysqli_fetch_all($res, MYSQLI_ASSOC);
        return $ret;
    }

    /**
     * SHOW CALENDARIO
     */
    function showCalendario() {
        $content = "<table class='striped'>";
        $content .= $this->getTableHead();
        $content .= $this->getBody();
        $content .= "</table>";
        return $content;
    }

    /**
     * Headtabella
     * @return string
     */
    private function getTableHead() {
        $content = "<thead class='center'>";
        $content .= $this->getDaysOnCalendar();
        $content .= "</thead>";
        return $content;
    }

    /**
     * Costruisce il numero di giorni
     * @return string
     */
    function getDaysOnCalendar() {
        if (isset($_REQUEST['date'])) {
            $date = date_create($_REQUEST['date']);
        } else {
            $date = new DateTime();
        }
        $content = "<tr><th class='orario center'>Orario</th>";
        $giorni = $this->dayCalendar;
        $date->modify('-1 day');
        for ($i = 0; $i < $giorni; $i++) {
            $date->modify('+1 day');
            $content .= "<th class='center giorno'>"
                    . strftime('%a %d %b', $date->getTimestamp())
                    . "</th>";
        }
        $content .= "</tr>";
        return $content;
    }

    //END TABLE HEAD

    /**
     * Corpo del tabellone
     * @return string
     */
    function getBody() {
        $content = "<tbody>";
        $tNow = strtotime($this->min);
        $tEnd = strtotime($this->max);
        while ($tNow <= $tEnd) {
            $content .= '<tr>'
                    . '<td class="orario center">';
            $content .= date("H:i", $tNow) . " - ";
            $tMid = date("H/i", $tNow);
            $tNow = strtotime('+30 minutes', $tNow);
            $content .= date("H:i", $tNow);
            $content .= '</td>';
            $content = $this->riempiBottoni($content, $tMid);
            $content .= '</tr>';
        }
        $content .= "</tbody>";
        return $content;
    }

    /**
     * Riempie ogni spazio della tabella
     * @param string $content
     * @param int $day
     * @param int $month
     * @param int $year
     * @return string
     */
    function riempiBottoni($content, $tMid) {
        if (isset($_REQUEST['date'])) {
            $date = date_create($_REQUEST['date']);
        } else {
            $date = new DateTime();
        }
        for ($i = 0; $i < $this->dayCalendar; $i++) {
            //$content .= "<td>cccc</td>";
            //if ($this->canWalk($x[0])) {
            $content .= "<td>" . $this->isWalkable($date->format('Y'), $date->format('m'), $date->format('d'), $tMid) . "</td>";
            $date->modify("+1 day");
            //}
        }
        return $content;
    }

    /**
     * Controlla se il user session può prenotare il campo
     * @param type $tariffa
     * @return boolean
     */
    private function canSub($tariffa) {
        $check = false;
        $arr = $this->getTariffa($tariffa);
        $privi = $this->priv;
        if ($privi != "guest") {
            foreach ($arr as $x) {
                if ($x["$privi"] != NULL || is_numeric($x["$privi"])) {
                    $check = true;
                }
            }
        }
        return $check;
    }

    //AAAAAAAAAAAAAA
    function getAAAA() {
        $check = array();
        $arr = $this->getTariffa("1");
        $privi = $this->priv;
        if ($privi != "guest") {
            foreach ($arr as $x) {
                if ($x["$privi"] != NULL) {
                    $check = true;
                }
            }
        }
        return $check;
    }

    /**
     * Controllo data per il periodo
     * @param type $id
     * @param type $data
     * @return boolean
     */
    function getPeriodoCampo($date) {
        $checkDate = new DateTime($date);
        $time = array();
        foreach ($this->periodi as $x) {
            if ($x['id_campo'] == $this->campo['cid']) {
                array_push($time, array($x['id_periodo'], $x['inizio_tariffa']));
            }
        }
        $len = count($time);
        $k = 0;
        $fix = array();
        foreach ($time as $x) {
            if ($k == 0) {
                $fine = new DateTime($x[1]);
                $fine->modify("-1 day");
                if ($x[1] != "01/01") {
                    array_push($fix, array("periodo" => $time[$len - 1][0], "inizio" => "01/01", "fine" => $fine->format("m/d")));
                    $fine = new DateTime($time[1][1]);
                    $fine->modify("-1 day");
                    array_push($fix, array("periodo" => $x[0], "inizio" => $x[1], "fine" => $fine->format("m/d")));
                } else {
                    $fine = new DateTime($time[1][1]);
                    $fine->modify("-1 day");
                    array_push($fix, array("periodo" => $x[0], "inizio" => $x[1], "fine" => $fine->format("m/d")));
                }
            } elseif ($k == $len - 1) {
                array_push($fix, array("periodo" => $time[$k][0], "inizio" => $x[1], "fine" => "12/31"));
            } else {
                $fine = new DateTime($time[$k + 1][1]);
                $fine->modify("-1 day");
                array_push($fix, array("periodo" => $x[0], "inizio" => $x[1], "fine" => $fine->format("m/d")));
            }
            $k++;
        }
        foreach ($fix as $x) {
            $lowerBound = new DateTime($x['inizio']);
            $upperBound = new DateTime($x['fine']);
            if ($lowerBound < $upperBound) {
                if ($lowerBound <= $checkDate && $checkDate <= $upperBound) {
                    return $x['periodo'];
                }
            } else {
                if ($checkDate <= $upperBound || $checkDate >= $lowerBound) {
                    return $x['periodo'];
                }
            }
        }
        return false;
    }

    /**
     * Controlla se quel slot è attivo oppure no
     * @param type $campo
     * @param type $data
     * @param type $ora
     * @param type $periodo
     * @return boolean
     */
    function loadCalendarioCampo($data, $ora, $periodo) {
        $weekday = date("w", $data);
        $arr = array();
        foreach ($this->setCalendario($this->campo['cid'], $periodo) as $x) {
            $checkTime = strtotime($x['ora']);
            if ($ora == $checkTime) {
                if ($x["$weekday"]) {
                    return $x["$weekday"];
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * Check tempo sul bottone
     * @param type $year
     * @param type $week
     * @param type $day
     * @param type $time
     * @param type $j
     * @return string
     */
    function isWalkable($year, $week, $day, $time) {
        $tim = explode("/", $time);
        $uDCheck = strtotime($week . "/" . $day . "/" . $year);
        $uHCheck = strtotime($tim[0] . ":" . $tim[1] . ":00");
        $tOra = date_create();
        $uDOra = strtotime(date_format($tOra, 'm/d/Y'));
        $uHOra = strtotime(date_format($tOra, 'H:i:s'));
        $periodo = $this->getPeriodoCampo("$week/$day");
        if (!$periodo) {
            //return "<a class='btn disabled center'>Chiuso</a>";
            return "";
        } else {
            if ($uDCheck < $uDOra) {
                //return "<a class='btn disabled center'>Passato</a>";
                return "";
            } else {
                if ($uDCheck == $uDOra) {
                    if ($uHCheck < $uHOra) {
                        //return "<a class='btn disabled center'>Passato</a>";
                        return "";
                    } else {
                        $tariffa = $this->loadCalendarioCampo($uDCheck, $uHCheck, $periodo);
                        if ($tariffa != NULL) {
                            if ($this->canSub($tariffa)) {
                                return $this->checkReservation($year, $week, $day, $time, $tariffa);
                            } else {
                                //return "<a class='btn center blue'>$tariffa</a>";
                                return "";
                            }
                        } else {
                            //return "<a class='btn disabled center'>Chiuso</a>";
                            return "";
                        }
                    }
                } else {
                    $tariffa = $this->loadCalendarioCampo($uDCheck, $uHCheck, $periodo);
                    if ($tariffa != NULL) {
                        if ($this->canSub($tariffa)) {
                            return $this->checkReservation($year, $week, $day, $time, $tariffa);
                        } else {
                            //return "<a class='btn center red'>$tariffa</a>";
                            return "";
                        }
                    } else {
                        //return "<a class='btn disabled center'>Chiuso</a>";
                        return "";
                    }
                }
            }
        }
    }

    /**
     * Check delle prenotazioni in quel campo/data
     * @param type $year
     * @param type $week
     * @param type $day
     * @param type $time
     * @param type $j
     * @return string
     */
    function checkReservation($year, $week, $day, $time, $tariffa) {
        $campo = $this->campo['cid'];
        $query = "SELECT * FROM prenotazioni WHERE reservation_year = '$year' AND reservation_week = '$week' AND reservation_day = '$day' AND reservation_time = '$time' AND reservation_campo = '$campo' ";
        $res = wrap_db_query($query);
        $result = mysqli_fetch_all($res, MYSQLI_ASSOC);
        if ($result != null) {
            if ($result[0]['reservation_deleted'] != 0) {
                return "<a class='btn waves-effect " . $this->getColoreTariffa($tariffa) . " waves-light btnprenota center' href='#prenotamodal' value='$year/$week/$day/$time/$campo/$tariffa/" . $result[0]['reservation_attivita'] . "'> Disp. " . $int . " posti</a>";
            } else {
                $count = $this->attivita->getMaxUserById($result[0]['reservation_attivita']);
                $int = $count - count($result) - $result[0]['reservation_guest'];
                if ($result[0]['reservation_user_id'] == $this->getUserId()) {
                    return "<a class='btn propriaprenotazione red' value='$year/$week/$day/$time/$campo'>Prenotato</a>";
                } else {
                    if ($int <= 0) {
                        return "<a class='btn occupato red' value='$year/$week/$day/$time/$campo'> Occupato </a>";
                    } else {
                        // TODO: check se può iscriversi / prenotare
                        return "<a class='btn waves-effect " . $this->getColoreTariffa($tariffa) . " waves-light btnprenota center' href='#prenotamodal' value='$year/$week/$day/$time/$campo/$tariffa/" . $result[0]['reservation_attivita'] . "'> Disp. " . $int . " posti</a>";
                    }
                }
            }
        } else {
            return "<a class='btn waves-effect " . $this->getColoreTariffa($tariffa) . " waves-light btnprenota center' href='#prenotamodal' value='$year/$week/$day/$time/$campo/$tariffa'> Prenota </a>";
        }
    }

    private function getUserId() {
        $db_query = "SELECT uid FROM users WHERE username = '" . $_SESSION['user'] . "'";
        $result = wrap_db_query($db_query);
        $res = mysqli_fetch_assoc($result);
        return $res['uid'];
    }

}
