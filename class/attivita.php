<?php

class Attivita {

    private $attivita = array();

    /**
     * Costruttore
     */
    public function __construct($conn) {
        setlocale(LC_TIME, 'ita', 'it_IT');
        $this->conn = $conn;
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $this->setAttivita();
    }

    function setAttivita() {
        $db_query = "SELECT * FROM attivita";
        $result = wrap_db_query($db_query);
        $this->attivita = mysqli_fetch_all($result);
    }
/*
    function canSub($campo) {
        $content = "";
        $int = 0;
        foreach ($this->attivita as $x) { //[1, 1, singolo, 1, 0, (1,2)]
            if ($x[0] == $campo) {
                $priv = $this->getPrivileges();
                if (in_array($priv, explode(",", $x[5])) || $priv == 0) {
                    if ($int != 0) {
                        $content .= "/";
                    }
                    $content .= "$x[1]";
                    $int++;
                }
            }
        }
        return $content;
    }
    */
    function getColorAttivita($id){
        switch ($id) {
            case 1: return "blue";
                break;
            case 2: return "orange";
                break;
            case 3: return "purple";
                break;
            default: return "green";
                break;
        }
    }

    function getMaxUserById($id) {
        foreach ($this->attivita as $x) {
            if ($x[0] == $id) {
                return $x[2];
            }
        }
    }

    public function getAttivita() {
        return $this->attivita;
    }

    function getAttivitaById($id) {
        foreach ($this->attivita as $x) {
            if ($x[0] == $id) {
                return $x[1];
            }
        }
    }

    function getPrivileges() {
        $query = "SELECT tid FROM anagrafica, users WHERE username = '" . $_SESSION['user'] . "' AND anagrafica.uid = users.uid";
        $res = wrap_db_query($query);
        $result = wrap_db_fetch_array($res);
        return "" . $result['tid'];
    }

}
