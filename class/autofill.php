<?php

class Autofill {

    private $utenti, $prenota;

    public function __construct() {
        include "../includes/database.php";
        setlocale(LC_TIME, 'ita', 'it_IT');
        $conn = wrap_db_connect();
        //include '../class/prenota.php';
        //$this->prenota = new Prenota();
        $this->setUtenti();
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
    }

    public function setUtenti() {
        $db_query = "SELECT uid, unome, ucognome FROM anagrafica";
        $res = wrap_db_query($db_query);
        $this->utenti = mysqli_fetch_all($res, MYSQLI_ASSOC);
    }

    public function getUtenti($val) {

        $year = $val[0];
        $week = $val[1];
        $day = $val[2];
        $h = $val[3];
        $m = $val[4];
        $time = $val[3] . "/" . $val[4];
        $campo = $val[5];
        $tariffa = $val[6];
        $tipo = $val[7];
        $arr = [];
        foreach ($this->utenti as $x) {
                $nome = $x['unome'] . " " . $x['ucognome'];
                $arr[] = [$x['uid'], $nome];
        }
        return json_encode($arr);
    }

}
