<?php

//CLASS

class Prenota {

    private $periodi, $tariffaid, $tariffatab, $nomeattivita, $idattivita, $prodotti, $extra, $otheruser, $idguest;

    public function __construct() {
        include "../includes/database.php";
        setlocale(LC_TIME, 'ita', 'it_IT');
        $conn = wrap_db_connect();
        include "attivita.php";
        $this->attivita = new Attivita($conn);
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $this->setPeriodi();
        $this->getIdGuest();
    }

    //GET E SET
    function getIdGuest() {
        $db_query = "SELECT tid FROM tipologia WHERE tnome = 'Guest'";
        $res = wrap_db_query($db_query);
        $aaa = mysqli_fetch_assoc($res);
        $this->idguest = $aaa['tid'];
    }

    private function setPeriodi() {
        $db_query = "SELECT * FROM tariffe_date";
        $res = wrap_db_query($db_query);
        $this->periodi = mysqli_fetch_all($res);
    }

    function getMaxUser($id) {
        return $this->attivita->getMaxUserById($id);
    }

    private function setAttivita() {
        $arr = array();
        foreach ($this->tariffatab as $x) {
            if ($x[$this->getPrivileges()] != NULL) {
                array_push($arr, array($x['id'], $x[$this->getPrivileges()]));
            }
        }
        return $arr;
    }

    private function getProdottoNomeById($id) {
        foreach ($this->prodotti as $x) {
            if ($x[0] == $id) {
                return $x[1];
            }
        }
    }

    function getNiceDate($anno, $mese, $giorno, $h, $m) {
        $date = new DateTime($anno . "-" . $mese . "-" . $giorno . " " . $h . ":" . $m);
        $content = "<p class='center'>" . strftime('%a %d %B %Y', $date->getTimestamp());
        $content .= "<br>" . $date->format('H:i') . " a ";
        $date->modify('+30 minute');
        $content .= "" . $date->format('H:i') . "</p>";
        return $content;
    }

    function getCampoNome($campo) {
        $db_query = "SELECT cnome FROM campo WHERE cid = '$campo'";
        $result = wrap_db_query($db_query);
        $aaa = mysqli_fetch_assoc($result);
        return $aaa['cnome'];
    }

    function getNomeAttivita() {
        return $this->nomeattivita;
    }

    function getPrezzo() {
        foreach ($this->tariffatab as $x) {
            if ($x['id'] == $this->idattivita) {
                $_SESSION['prezzototale'] = $x[$this->getPrivileges()];
                return $x[$this->getPrivileges()];
            }
        }
    }

    /**
     * Carica il calendario settimanale del campo in quel periodo
     * @param type $id
     * @param type $periodo
     * @return type
     */
    function setCalendario($id, $periodo) {
        $db_query = "SELECT * FROM cal_camp$id" . "_pe$periodo";
        $res = wrap_db_query($db_query);
        $ret = mysqli_fetch_all($res, MYSQLI_ASSOC);
        return $ret;
    }

    private function setTariffeTab($tariffa) {
        $this->tariffaid = $tariffa;
        $db_query = "SELECT * FROM tariffe_$tariffa";
        $result = wrap_db_query($db_query);
        $this->tariffatab = mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    function getPrivileges() {
        $query = "SELECT tid FROM anagrafica, users WHERE username = '" . $_SESSION['user'] . "' AND anagrafica.uid = users.uid";
        $res = wrap_db_query($query);
        $result = wrap_db_fetch_array($res);
        return $result['tid'];
    }

    function setOtherUser($str) {
        //if($str != "") {
        $arr = explode(",", $str);
        $this->otheruser = $arr;
        //}
    }

    private function setProdotti() {
        $db_query = "SELECT * FROM extra";
        $res = wrap_db_query($db_query);
        if ($res) {
            $this->prodotti = mysqli_fetch_all($res);
            return true;
        } else {
            return false;
        }
    }

    function setProdottiTab() {
        $db_query = "SELECT * FROM extra_" . $this->tariffaid;
        $res = wrap_db_query($db_query);
        return mysqli_fetch_all($res, MYSQLI_ASSOC);
    }

    // END GET E SET

    /**
     * Se lo slot è prenotabile
     * @param type $year
     * @param type $week
     * @param type $day
     * @param type $time
     * @param type $campo
     * @return boolean
     */
    function isWalkable($year, $week, $day, $time, $campo) {
        $check = strtotime($week . "/" . $day . "/" . $year);
        $tim = explode("/", $time);
        $checkOra = strtotime($tim[0] . ":" . $tim[1] . ":00");
        $periodo = $this->getPeriodoCampo($campo, "$week/$day");
        if ($periodo) {
            $tariffa = $this->loadCalendarioCampo($campo, $check, $checkOra, $periodo);
            if ($tariffa) {
                $this->setTariffeTab($tariffa);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Controllo data per il periodo
     * @param type $id
     * @param type $data
     * @return boolean
     */
    function getPeriodoCampo($id, $data) {
        $checkDate = new DateTime($data);
        foreach ($this->periodi as $x) {
            if ($x[1] == $id) {
                $lowerBound = new DateTime($x[3]);
                $upperBound = new DateTime($x[4]);
                if ($lowerBound < $upperBound) {
                    if ($lowerBound <= $checkDate && $checkDate <= $upperBound) {
                        $this->extra = $x[5];
                        return $x[2];
                    }
                } else {
                    if ($checkDate <= $upperBound || $checkDate >= $lowerBound) {
                        $this->extra = $x[5];
                        return $x[2];
                    }
                }
            }
        }
        return false;
    }

    /**
     * Controlla se quel slot è attivo oppure no
     * @param type $campo
     * @param type $data
     * @param type $ora
     * @param type $periodo
     * @return boolean
     */
    function loadCalendarioCampo($campo, $data, $ora, $periodo) {
        $weekday = date("w", $data);
        $arr = array();
        foreach ($this->setCalendario($campo, $periodo) as $x) {
            $checkTime = strtotime($x['ora']);
            if ($ora == $checkTime) {
                if ($x["$weekday"]) {
                    return $x["$weekday"];
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * Controlla se lo slot è riservato
     * @param type $year
     * @param type $week
     * @param type $day
     * @param type $time
     * @param type $campo
     * @return boolean
     */
    function checkReservation($year, $week, $day, $time, $campo) {
        $query = "SELECT * FROM prenotazioni WHERE reservation_year = '$year' AND reservation_week = '$week' AND reservation_day = '$day' AND reservation_time = '$time' AND reservation_campo = '$campo' ";
        $res = wrap_db_query($query);
        $result = mysqli_fetch_all($res);
        if ($result != null) {
            $count = $this->attivita->getMaxUserById($result[0][12]);
            $this->nomeattivita = $this->attivita->getAttivitaById($result[0][12]);
            $this->idattivita = $result[0][12];
            $int = $count - count($result) - $result[0][13];
            if (count($result) >= $count) {
                return false;
            } else {
                if ($this->canSub()) {
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    /**
     * Controlla se può iscriversi
     * @param type $tariffa
     * @return boolean
     */
    function canSub() {
        $check = false;
        $privi = $this->getPrivileges();
        if ($privi != "guest") {
            foreach ($this->tariffatab as $x) {
                if ($x["$privi"]) {
                    $check = true;
                }
            }
        }
        return $check;
    }

    /**
     * Controlla se lo spazio è un evento
     * @param type $year
     * @param type $week
     * @param type $day
     * @param type $time
     * @param type $id
     * @return boolean
     */
    function isEvento($year, $week, $day, $time, $id) {
        $query = "SELECT * FROM prenotazioni WHERE reservation_year = '$year' AND reservation_week = '$week' AND reservation_day = '$day' AND reservation_time = '$time' AND reservation_campo = '$id' ";
        $res = wrap_db_query($query);
        $result = mysqli_fetch_all($res);
        if ($result != null) {
            return true;
        } else {
            return false;
        }
    }

    function getBody() {
        $content = "";
        if ($this->setProdotti()) {
            $content = "<table class='striped col s6'>"
                    . "<tbody>";
            foreach ($this->setProdottiTab() as $x) {
                if ($x[$this->getPrivileges()] != NULL) {
                    $content .= "<tr> <td> <input type='checkbox' class='filled-in prodotto";
                    if ($this->extra == $x['id']) {
                        $content .= "disabled' checked = 'true";
                    }
                    $content .= "' id='prodotto" . $x['id'] . "' value='" . $x['id'] . "' />"
                            . "<label for='prodotto" . $x['id'] . "'>" . $this->getProdottoNomeById($x['id']) . "</label> </td>"
                            . "<td>" . $x[$this->getPrivileges()] / 100 . " €</td></tr>";
                }
            }
            $content .= "</tbody>"
                    . "</table>";
            $content .= "<table class='striped col s6'>";
        } else {
            $content .= "<table class='striped col s6 offset-s3'>";
        }
        $content .= "<tbody>";
        foreach ($this->setAttivita() as $x) {
            $content .= "<tr> <td><input type='radio' name='attivita' class='filled-in attivita' id='attivita" . $x[0] . "' value='" . $x[0] . "'>"
                    . "<label for='attivita" . $x[0] . "'>" . $this->attivita->getAttivitaById($x[0]) . "</label></td>"
                    . "<td>" . $x[1] / 100 . " €</td></tr>";
        }
        $content .= "</tbody></table>";
        return $content;
    }

    function getFooter($val) {
        $content = "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnmodnext' value='" . $val . "'>Avanti</a></div>";
        return $content;
    }

    private function getPrezzoAttivita($id) {
        foreach ($this->setAttivita() as $x) {
            if ($x[0] == $id) {
                return $x[1] / 100;
            }
        }
    }

    private function getPrezzoExtra($id) {
        foreach ($this->setProdottiTab() as $x) {
            if ($x['id'] == $id) {
                return array($this->getProdottoNomeById($x['id']), $x[$this->getPrivileges()] / 100);
            }
        }
    }

    function getRiassunto($attivita, $extra, $utenti) {
        $ute = explode(",", $utenti);
        $guests = 0;
        foreach ($ute as $x) {
            if ($x == 0) {
                $guests++;
            }
        }
        $prezzototale = 0;
        $this->setProdotti();
        $extras = explode(",", $extra);
        $prezzoextra = array();
        $prezzoattivita = $this->getPrezzoAttivita($attivita);
        $content = "<div class='row'>";
        $content .= "<p> " . $this->attivita->getAttivitaById($attivita) . " - ";
        if ($guests == 0) {
            $content .= $prezzoattivita;
            $prezzototale = $prezzototale + $prezzoattivita;
        } else {
            $content .= $prezzoattivita + ($this->getAttivitaPrezzoGuest($attivita) * $guests);
            $prezzototale = $prezzototale + $prezzoattivita + ($this->getAttivitaPrezzoGuest($attivita) * $guests);
        }
        $content .= " € </p> ";
        $prezzoextras = 0;
        if ($extras[0] != " ") {
            foreach ($extras as $x) {
                $content .= "<p>";
                $result = $this->getPrezzoExtra($x);
                $content .= $result[0] . " - " . $result[1] . " €";
                $content .= "</p>";
                $prezzototale = $prezzototale + $result[1];
            }
        }
        $content .= "<hr> <p> Totale: $prezzototale € </p>";
        $_SESSION['prezzototale'] = $prezzototale * 100;
        $content .= "</div>";
        return $content;
    }

    function getAttivitaPrezzoGuest($id) {
        $arr = array();
        foreach ($this->tariffatab as $x) {
            if ($x['id'] == $id) {
                if ($x[$this->idguest] != NULL) {
                    return $x[$this->idguest];
                }
            }
        }
    }

    function getFooterEvento($val) {
        $content = "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnmodprenota' value='" . $val . "'>Prenota</a></div>";
        return $content;
    }

    function getUser() {
        return $this->otheruser;
    }

    function getFooterIscrizioneEvento($val) {
        $content = "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnmodprenotaev' value='" . $val . "'>Prenota</a></div>";
        return $content;
    }

    function getFooterFill($val) {
        $content = "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnmodfill' value='" . $val . "'>Avanti</a></div>";
        return $content;
    }

    function setPrenota($year, $week, $day, $time, $campo, $prodotti, $attivita, $tariffa, $utenti) {
        $query = "";
        $arr = explode(",", $utenti);
        $check = false;
        $i = 0;
        foreach ($arr as $x) {
            if ($x == "0") {
                $i++;
            }
        }
        if ($i == $this->getMaxUser($attivita)) {
            $query = "INSERT INTO prenotazioni (reservation_made_time, reservation_year, reservation_week, reservation_day, reservation_time, reservation_campo, reservation_user_id, reservation_prod, reservation_attivita, reservation_tariffa, reservation_guest, reservation_price) "
                    . " VALUES (now(), $year, $week, $day, '$time', $campo, 1, '$prodotti', '$attivita', $tariffa, $i, ".$_SESSION['prezzototale'].") ";
            $res = wrap_db_query($query);
            if ($res) {
                $check = true;
            } else {
                $check = false;
            }
        } else {
            foreach ($arr as $x) {
                if ($x != 0) {
                    if ($i != 0) {
                        $query = "INSERT INTO prenotazioni (reservation_made_time, reservation_year, reservation_week, reservation_day, reservation_time, reservation_campo, reservation_user_id, reservation_prod, reservation_attivita, reservation_tariffa, reservation_guest, reservation_price) "
                                . " VALUES (now(), $year, $week, $day, '$time', $campo, $x, '$prodotti', '$attivita', $tariffa, $i, ".$_SESSION['prezzototale'].") ";
                        $i = 0;
                    } else {
                        $query = "INSERT INTO prenotazioni (reservation_made_time, reservation_year, reservation_week, reservation_day, reservation_time, reservation_campo, reservation_user_id, reservation_prod, reservation_attivita, reservation_tariffa, reservation_price) "
                                . " VALUES (now(), $year, $week, $day, '$time', $campo, $x, '$prodotti', '$attivita', $tariffa, ".$_SESSION['prezzototale'].") ";
                    }
                    $res = wrap_db_query($query);
                    if ($res) {
                        $check = true;
                    } else {
                        $check = false;
                    }
                }
            }
        }
        if ($check) {
            return "Prenotato";
        } else {
            return "Errore";
        }
    }

    function getCurrentUserId() {
        $user = $_SESSION['user'];
        $db_query = "SELECT users.uid FROM anagrafica, users WHERE anagrafica.uid = users.uid AND users.username = '$user'";
        $result = wrap_db_query($db_query);
        $res = mysqli_fetch_row($result);
        return $res[0];
    }

    function chipCurrentUser() {
        $user = $_SESSION['user'];
        $db_query = "SELECT users.uid, unome, ucognome FROM anagrafica, users WHERE anagrafica.uid = users.uid AND users.username = '$user'";
        $result = wrap_db_query($db_query);
        $res = mysqli_fetch_row($result);
        if ($this->getPrivileges() != 0) {
            return "<div class='chip' value='$res[0]'>" . $res[1] . " " . $res[2] . "</div>";
        }
    }

    function getChipRiassunto($utenti) {
        $arr = explode(",", $utenti);
        $content = "";
        foreach ($arr as $x) {
            if ($x == 0) {
                $content .= "<div class='chip' value='0'>Guest</div>";
            } else {

                $db_query = "SELECT unome, ucognome FROM anagrafica WHERE uid = $x ;";
                $result = wrap_db_query($db_query);
                $res = mysqli_fetch_row($result);
                $content .= "<div class='chip' value='$x'>$res[0] $res[1]</div>";
            }
        }
        return $content;
    }

    function getLookup($year, $week, $day, $hour, $min, $campo) {
        $db_query = "SELECT reservation_pagato, reservation_prod, anome, tariffa, unome, ucognome, reservation_guest"
                . " FROM prenotazioni as p, tariffe as f, anagrafica as a, attivita as att"
                . " WHERE reservation_year = '$year' AND reservation_week = '$week' AND reservation_day = '$day' AND reservation_time = '$hour/$min' AND reservation_campo = $campo"
                . " AND reservation_user_id = a.uid AND reservation_tariffa = f.id AND att.aid = reservation_attivita";

        $result = wrap_db_query($db_query);
        $res = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $prod = explode(",", $res[0]['reservation_prod']);
        $nomprod = array();
        if ($res[0]['reservation_prod'] != NULL) {
            foreach ($prod as $x) {
                $db_query = "SELECT nome FROM extra WHERE id = $x";
                $rez = wrap_db_query($db_query);
                $proda = mysqli_fetch_assoc($rez);
                array_push($nomprod, $proda['nome']);
            }
        } else {
            array_push($nomprod, "Nessuno");
        }
        $content = "<table class='striped centered col s8'>"
                . "<thead> <th> Chi </th> <th> Tariffa </th> <th> Attività </th> <th> Prodotti </th> <th> Pagato </th> <th> Guests </th> </thead>"
                . "<tbody>";
        foreach ($res as $x) {
            $content .= "<tr>"
                    . "<td>" . $x['unome'] . " " . $x['ucognome'] . "</td>"
                    . "<td>" . $x['tariffa'] . "</td>"
                    . "<td>" . $x['anome'] . "</td>"
                    . "<td>";
            foreach ($nomprod as $y) {
                $content .= $y . " ";
            }
            $content .= "</td>"
                    . "<td>";
            if ($x['reservation_pagato']) {
                $content .= "Si";
            } else {
                $content .= "No";
            }
            $content .= "</td>";
            if ($x['reservation_guest'] != 0) {
                $content .= "<td>" . $x['reservation_guest'] . "</td>";
            } else {
                $content .= "<td></td>";
            }
            $content .= "</tr>";
        }
        $content .= "</tbody></table>";
        return $content;
    }

    function deletePrenotazione($year, $week, $day, $h, $m, $campo) {
        $db_query = "DELETE FROM prenotazioni WHERE reservation_year = '$year' AND reservation_week = '$week' AND reservation_day = '$day' AND reservation_time = '$h/$m' "
                . "AND reservation_campo = '$campo'";
        $res = wrap_db_query($db_query);
    }

}
