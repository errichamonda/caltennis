<?php

//SETTINGS CAMPO

class Setting {

    private $campi, $tariffe, $attivita, $prodotti, $tipologia, $utenti, $prenotazioni;

    /**
     * Costruttore
     */
    public function __construct() {
        include "../includes/database.php";
        setlocale(LC_TIME, 'ita', 'it_IT');
        $conn = wrap_db_connect();
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
    }

    //CAMPI {

    function setCampi() {
        $db_query = "SELECT * FROM campo";
        $result = wrap_db_query($db_query);
        $res = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $this->campi = $res;
    }

    /**
     * Costruzione involucro tabella
     * @return string
     */
    public function getCampi() {
        $content = "<div class='container'>"
                . "<ul class='collapsible popout' data-collapsible='accordion'>";
        foreach ($this->campi as $x) {
            $content .= "<li>"
                    . "<div class='collapsible-header'>" . $x['cnome'] . "</div>"
                    . "<div class='collapsible-body'>" . $this->getTabella($x['cid']) . "</p>"
                    . "<div class='right-align'><a data-position='bottom' data-delay='50' data-tooltip='Nuovo periodo' class='btn-floating tooltipped waves-effect waves-light red addperiodo' value='" . $x['cid'] . "'><i class='material-icons'>add</i></a>"
                    . "&nbsp;<a class='btn-floating waves-effect waves-light green saveperiodo tooltipped' data-position='bottom' data-delay='50' data-tooltip='Salva calendario' value='" . $x['cid'] . "'><i class='material-icons'>save</i></a></div></div>"
                    . "</li>";
        }
        $content .= "</ul>"
                . "</div>";
        return $content;
    }

    /**
     * Tabella informazioni campo
     * @param type $id
     * @return string
     */
    private function getTabella($id) {
        $db_query = "SELECT * FROM periodi WHERE id_campo = $id ORDER BY inizio_tariffa ASC";
        $result = wrap_db_query($db_query);
        $res = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $content = "<table class='striped centered' value='$id'>"
                . "<thead><th>Periodo</th><th>DataInizio</th> <th>DataFine</th><th>Extra</th><th>Modifica Tariffe</th></thead>"
                . "<tbody>";
        $len = count($res);
        $i = 0;
        foreach ($res as $x) {
            if ($i == 0) {
                $fine = new DateTime($res[1]['inizio_tariffa']);
                $fine->modify("-1 day");
                $inizio = new DateTime($x['inizio_tariffa']);
                $content .= "<tr value='" . $x['id'] . "'> <td>" . $x['id_periodo'] . "</td> <td>" . strftime('%d %B', $inizio->getTimestamp()) . "</td> <td>" . strftime('%d %B', $fine->getTimestamp()) . "</td><td>" . $x['extra'] . "</td>"
                        . "<td><a class='btn btn-flat waves-effect center calendariotariffe'>Modifica</a>"
                        . "<a class='btn btn-flat waves-effect center eliminacalendariotariffe'>Elimina</a>"
                        . "<a class='btn btn-flat waves-effect center copiacalendariotariffe'>Copia</a>"
                        . "</td></tr>";
            } elseif ($i == $len - 1) {
                $fine = new DateTime($res[0]['inizio_tariffa']);
                $fine->modify("-1 day");
                $inizio = new DateTime($x['inizio_tariffa']);
                $content .= "<tr value='" . $x['id'] . "'> <td>" . $x['id_periodo'] . "</td> <td>" . strftime('%d %B', $inizio->getTimestamp()) . "</td>  <td>" . strftime('%d %B', $fine->getTimestamp()) . "</td><td>" . $x['extra'] . "</td>"
                        . "<td><a class='btn btn-flat waves-effect center calendariotariffe'>Modifica</a>"
                        . "<a class='btn btn-flat waves-effect center eliminacalendariotariffe'>Elimina</a>"
                        . "<a class='btn btn-flat waves-effect center copiacalendariotariffe'>Copia</a>"
                        . "</td></tr>";
            } else {
                $fine = new DateTime($res[$i + 1]['inizio_tariffa']);
                $fine->modify("-1 day");
                $inizio = new DateTime($x['inizio_tariffa']);
                $content .= "<tr value='" . $x['id'] . "'> <td>" . $x['id_periodo'] . "</td> <td>" . strftime('%d %B', $inizio->getTimestamp()) . "</td>  <td>" . strftime('%d %B', $fine->getTimestamp()) . "</td><td>" . $x['extra'] . "</td>"
                        . "<td><a class='btn btn-flat waves-effect center calendariotariffe'>Modifica</a>"
                        . "<a class='btn btn-flat waves-effect center eliminacalendariotariffe'>Elimina</a>"
                        . "<a class='btn btn-flat waves-effect center copiacalendariotariffe'>Copia</a>"
                        . "</td></tr>";
            }
            $i++;
        }
        $content .= "</tbody>"
                . "</table>";
        return $content;
    }

    function calendarioCampi($id, $periodo) {
        $db_query = "SELECT * FROM cal_camp" . $id . "_pe" . $periodo;
        $result = wrap_db_query($db_query);
        $res = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $content = "<h4 class='center'>Campo " . $this->getCampoNomeFromId($id) . " Periodo $periodo</h4>"
                . "<div class='row'>"
                . "<table class='striped centered col s9' id='campotable'><thead><th>Ora</th><th>Dom</th><th>Lun</th><th>Mar</th><th>Mer</th><th>Gio</th><th>Ven</th><th>Sab</th></thead>"
                . "<tbody>";
        foreach ($res as $x) {
            $time = substr($x['ora'], 0, 5);
            $content .= "<tr> <td> $time </td>";
            for ($i = 0; $i < 7; $i++) {
                $content .= "<td class='tar " . $this->getColoreTariffa($x[$i]) . "' vaid ='$x[$i]' >" . $this->getTariffaSiglaById($x[$i]) . "</td>";
            }
            $content .= "</tr>";
        }
        $content .= "</tbody></table>"
                . "<div class='col s3 carta'>"
                . "<div class='card darken-1'>"
                . "<div class='card-content white-text'>"
                . $this->getCalendarioCampiTariffeTab()
                . "<a class='btn-floating btn-large halfway-fab waves-effect green' id='savecalendariotariffa' periodo='$periodo' campo='$id'><i class='material-icons large'>save</i></a>"
                . "</div>"
                . "</div>"
                . "</div>"
                . "</div>";
        return $content;
    }

    private function getTariffaSiglaById($id) {
        if ($id == NULL) {
            return "";
        } else {
            $this->setTariffe();
            foreach ($this->tariffe as $x) {
                if ($x['id'] == $id) {
                    return $x['sigla'];
                }
            }
        }
    }

    private function getCalendarioCampiTariffeTab() {
        $this->setTariffe();
        $content = "<p> <input name='radiobuttontariffe' class='filled-in' checked='true' type='radio' id='radio0' value='' vaid='' colore=''/> <label for='radio0'>Chiuso</label></p>";
        foreach ($this->tariffe as $x) {
            $content .= "<p> <input name='radiobuttontariffe' class='filled-in' type='radio' id='radio" . $x['id'] . "' value='" . $x['sigla'] . "' vaid='" . $x['id'] . "' colore='" . $this->getColoreTariffa($x['id']) . "'/> <label for='radio" . $x['id'] . "'>" . $x['tariffa'] . " (" . $x['sigla'] . ")</label></p>";
        }
        return $content;
    }

    private function getColoreTariffa($id) {
        $this->setTariffe();
        foreach ($this->tariffe as $x) {
            if ($x['id'] == $id) {
                return $x['colore'];
            }
        }
    }

    private function getCampoNomeFromId($id) {
        $this->setCampi();
        foreach ($this->campi as $x) {
            if ($x['cid'] == $id) {
                return $x['cnome'];
            }
        }
    }

    function saveCalendarioTariffa() {
        $campo = $_REQUEST['campo'];
        $periodo = $_REQUEST['periodo'];
        $array = $_REQUEST['array'];
        $content = "";
        if (isset($_REQUEST['campo']) && isset($_REQUEST['periodo']) && isset($_REQUEST['array'])) {
            $db_query = "TRUNCATE TABLE cal_camp" . $campo . "_pe" . $periodo . "; ";
            $res = wrap_db_query($db_query);
            echo $db_query;
            foreach ($array as $x) {
                $db_query = "INSERT INTO cal_camp" . $campo . "_pe" . $periodo . " (`ora`, `0`, `1`, `2`, `3`, `4`, `5`, `6`) VALUES (";
                $i = 0;
                $len = count($x);
                foreach ($x as $y) {
                    if ($i == 0) {
                        $db_query .= "'$y', ";
                    } elseif ($i == $len - 1) {
                        if ($y == "") {
                            $db_query .= " NULL ); ";
                        } else {
                            $db_query .= " '$y' ); ";
                        }
                    } else {
                        if ($y == "") {
                            $db_query .= " NULL, ";
                        } else {
                            $db_query .= " '$y', ";
                        }
                    }
                    $i++;
                }
                $res = wrap_db_query($db_query);
                echo $db_query;
            }
        }
        return "Eseguito";
    }

    //END CAMPI
    // TARIFFE

    function setTariffe() {
        $db_query = "SELECT * FROM tariffe";
        $result = wrap_db_query($db_query);
        $res = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $this->tariffe = $res;
    }

    function getTariffe() {
        $this->setTariffe();
        $this->setAttivita();
        $content = "<div class='container'>"
                . "<ul class='collapsible popout' data-collapsible='accordion'>";
        foreach ($this->tariffe as $x) {
            $content .= "<li>"
                    . "<div class='collapsible-header'>" . $x['tariffa'] . "</div>"
                    . "<div class='collapsible-body'>" . $this->getTabellaTariffa($x['id'])
                    . "<div class='right-align'>"
                    . "<a class='btn-floating coloretariffa " . $this->getColoreTariffa($x['id']) . " tooltipped' data-position='bottom' data-delay='50' data-tooltip='Cambia colore' value='" . $this->getColoreTariffa($x['id']) . "' idtariffa='" . $x['id'] . "'><i class='material-icons'>color_lens</i></a>"
                    . " <a class='btn-floating waves-effect waves-light green savetariffatab tooltipped' data-position='bottom' data-delay='50' data-tooltip='Salva tariffa' value ='" . $x['id'] . "'><i class='material-icons'>save</i></a></div></div>"
                    . "</li>";
        }
        $content .= "</ul>"
                . "<div class='fixed-action-btn'>"
                . "<a class='btn-floating btn-large waves-effect waves-light red tooltipped' data-position='left' data-delay='50' data-tooltip='Nuova tariffa' id='newtariffamodal'><i class='large material-icons'>add</i></a>"
                . "</div>"
                . "</div>";
        return $content;
    }

    private function setAttivita() {
        $db_query = "SELECT * FROM attivita";
        $result = wrap_db_query($db_query);
        $this->attivita = mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    private function getAttivitaNomeById($id) {
        foreach ($this->attivita as $x) {
            if ($x['aid'] == $id) {
                return $x['anome'];
            }
        }
    }

    private function setTipologia() {
        $db_query = "SELECT * FROM tipologia";
        $result = wrap_db_query($db_query);
        $this->tipologia = mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    private function getTabellaTariffa($id) {
        $db_query = "SELECT * FROM tariffe_$id";
        $result = wrap_db_query($db_query);
        $tariffatab = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $this->setTipologia();
        $content = "<table class='striped centered' id='tariffa" . $id . "'>"
                . "<thead> <th value='id'> Attività </th>";
        foreach ($this->tipologia as $x) {
            $content .= "<th value='" . $x['tid'] . "'>" . $x['tnome'] . "</th>";
        }
        $content .= "</thead><tbody>";
        foreach ($tariffatab as $x) {
            $content .= "<tr><td value='" . $x['id'] . "'>" . $this->getAttivitaNomeById($x['id']) . "</td>";
            foreach ($this->tipologia as $y) {
                $content .= "<td class='setted' value='" . $y['tid'] . "'>" . $x[$y['tid']] . "</td>";
            }
            $content .= "</tr>";
        }
        $content .= "</tbody></table>";
        return $content;
    }

    function modificaTariffe($id, $arr) {
        $ids = array_shift($arr);
        $db_query = "DROP TABLE tariffe_" . $id;
        $res = wrap_db_query($db_query);
        $db_query = "CREATE TABLE tariffe_" . $id . " ( ";
        $i = 0;
        $len = count($ids);
        foreach ($ids as $x) {
            if ($i == 0) {
                $db_query .= "`$x` INT(11) PRIMARY KEY";
            } else {
                $db_query .= "`$x` INT(11) DEFAULT NULL";
            }
            if ($i != $len - 1) {
                $db_query .= ", ";
            }
            $i++;
        }
        $db_query .= "); ";
        $res = wrap_db_query($db_query);
        foreach ($arr as $x) {
            $db_query = "INSERT INTO tariffe_" . $id . " VALUES (";
            $i = 0;
            $len = count($x);
            foreach ($x as $y) {
                if ($y == "") {
                    $db_query .= "NULL";
                } else {
                    $db_query .= "'" . $y . "'";
                }
                if ($i != $len - 1) {
                    $db_query .= ", ";
                }
                $i++;
            }
            $db_query .= " ) ";
            $res = wrap_db_query($db_query);
        }
    }

    function modalNewTariffa() {
        $content = "<div class='modal-content'>"
                . "<h4 class='center'>Nuova tariffa</h4> <br>"
                . "<div class='row'>"
                . "<div class='input-field col s4 offset-s1'><input type='text' id='nomenewtariffe'><label for='nomenetaroffe'>Nome</label></div>"
                . "<div class='input-field col s4 offset-s2'><input type='text' id='shortnametariffa' data-length='3'><label for='shortnametariffa'>Sigla</label></div>"
                . "</div>"
                . "</div>"
                . "<div class='modal-footer'><a class='modal-action waves-effect waves-green btn-flat' id='newtariffa'>Crea</a></div>";
        return $content;
    }

    function newTariffa($nome, $sigla) {
        $db_query = "SELECT tariffa, sigla FROM tariffe WHERE tariffa = '$nome' OR sigla = '$sigla'";
        $res = wrap_db_query($db_query);
        $result = mysqli_fetch_assoc($res);
        if (strtolower($result['tariffa']) === strtolower($nome) || strtoupper($result['sigla']) === strtoupper($sigla)) {
            return "Duplicato";
        } else {
            $sigla = strtoupper($sigla);
            $nome = strtolower($nome);
            $db_query = "INSERT INTO tariffe (tariffa, sigla) VALUES ('$nome', '$sigla'); ";
            $res = wrap_db_query($db_query);
            $db_query = "SELECT id FROM tariffe WHERE tariffa = '$nome'";
            $res = wrap_db_query($db_query);
            $result = mysqli_fetch_assoc($res);
            $id = $result['id'];
            $db_query = "CREATE TABLE tariffe_" . $id . " ( `id` INT(11) PRIMARY KEY, ";
            $query = "SELECT tid FROM tipologia";
            $res = wrap_db_query($query);
            $result = mysqli_fetch_all($res);
            $i = 0;
            $len = count($result);
            foreach ($result as $x) {
                $db_query .= "`$x[0]` INT(11) DEFAULT NULL";
                if ($i != $len - 1) {
                    $db_query .= ", ";
                }
                $i++;
            }
            $db_query .= ")";
            $res = wrap_db_query($db_query);
            $db_query = "";
            $query = "SELECT aid FROM attivita";
            $res = wrap_db_query($query);
            $result = mysqli_fetch_all($res);
            foreach ($result as $x) {
                $db_query = "INSERT INTO tariffe_" . $id . " (`id`) VALUES ( " . $x[0] . " ); ";
                $res = wrap_db_query($db_query);
            }
            $db_query = "CREATE TABLE extra_" . $id . " ( `id` INT(11) PRIMARY KEY, ";
            $query = "SELECT tid FROM tipologia";
            $res = wrap_db_query($query);
            $result = mysqli_fetch_all($res);
            $i = 0;
            $len = count($result);
            foreach ($result as $x) {
                $db_query .= "`$x[0]` INT(11) DEFAULT NULL";
                if ($i != $len - 1) {
                    $db_query .= ", ";
                }
                $i++;
            }
            $db_query .= ")";
            $res = wrap_db_query($db_query);
            $query = "SELECT id FROM extra";
            $res = wrap_db_query($query);
            $result = mysqli_fetch_all($res);
            foreach ($result as $x) {
                $db_query = "INSERT INTO extra_" . $id . " (`id`) VALUES ( " . $x[0] . " ); ";
                $res = wrap_db_query($db_query);
            }
        }
    }

    function eliminaTariffa($campo, $id, $periodo) {
        if (is_numeric($campo) && is_numeric($periodo) && is_numeric($id)) {
            try {
                $db_query = "DELETE FROM `periodi` WHERE `id` = $id AND `id_campo` = $campo AND `id_periodo` = $periodo ";
                $result = wrap_db_query($db_query);
                $db_query .= "DROP TABLE `cal_camp" . $campo . "_pe" . $periodo."`";
                $result = wrap_db_query($db_query);
                return "Eseguito";
                //return $db_query;
            } catch (Exception $ex) {
                return "Errore";
            }
        } else {
            return "Errore";
        }
    }

    //END TARIFFE
    //PRODOTTI

    private function setProdotti() {
        $db_query = "SELECT * FROM extra";
        $result = wrap_db_query($db_query);
        $this->prodotti = mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    function getProdotti() {
        $this->setProdotti();
        $this->setTariffe();
        $content = "<div class='container'>"
                . "<ul class='collapsible popout' data-collapsible='accordion'>";
        $content .= "<li> <div class='collapsible-header'> Prodotti </div>"
                . "<div class='collapsible-body'>";
        $content .= "<table class='striped centered col s4 offset-s4'>"
                . "<thead> <th>Nome</th><th>Stato</th></thead><tbody>";
        foreach ($this->prodotti as $x) {
            $content .= "<tr value='" . $x['id'] . "'><td>" . $x['nome'] . "</td><td>" . $x['stato'] . "</td></tr>";
        }
        $content .= "</tbody></table>"
                . "<br><div class='right-align'><a class='btn-floating waves-effect waves-light red tooltipped' data-position='bottom' data-delay='50' data-tooltip='Nuovo Prodotto'' id='newprodotto'><i class='material-icons'>add</i></a></div>"
                . "</div></li>";
        foreach ($this->tariffe as $x) {
            $content .= "<li>"
                    . "<div class='collapsible-header'>" . $x['tariffa'] . "</div>"
                    . "<div class='collapsible-body'>" . $this->getTabellaProdotti($x['id'])
                    . "<br><div class='right-align'><a class='btn-floating waves-effect waves-light green saveextratab tooltipped' data-position='bottom' data-delay='50' data-tooltip='Salva tariffe' value='" . $x['id'] . "'><i class='material-icons'>save</i></a></div></div>"
                    . "</li>";
        }
        $content .= "</ul>"
                . "</div>";
        return $content;
    }

    private function getProdottoById($id) {
        foreach ($this->prodotti as $x) {
            if ($x['id'] == $id) {
                return $x['nome'];
            }
        }
    }

    private function getTabellaProdotti($id) {
        $db_query = "SELECT * FROM extra_$id";
        $result = wrap_db_query($db_query);
        $extra = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $this->setTipologia();
        $content = "<table class='striped centered' id='extra$id'>"
                . "<thead> <th value='id'> Prodotto </th>";
        foreach ($this->tipologia as $x) {
            $content .= "<th value ='" . $x['tid'] . "'>" . $x['tnome'] . "</th>";
        }
        $content .= "</thead><tbody>";
        foreach ($extra as $x) {
            $content .= "<tr><td value='" . $x['id'] . "'>" . $this->getProdottoById($x['id']) . "</td>";
            foreach ($this->tipologia as $y) {
                $content .= "<td class='setted' value='" . $y['tid'] . "'>" . $x[$y['tid']] . "</td>";
            }
            $content .= "</tr>";
        }
        $content .= "</tbody></table>";
        return $content;
    }

    function getPrivileges() {
        $query = "SELECT tid FROM anagrafica, users WHERE username = '" . $_SESSION['user'] . "' AND anagrafica.uid = users.uid";
        $res = wrap_db_query($query);
        $result = wrap_db_fetch_array($res);
        if ($result['tid'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * MODAL PRODOTTO NUOVO
     */
    private function getIdTariffe() {
        $db_query = "SELECT id FROM tariffe";
        $res = wrap_db_query($db_query);
        return mysqli_fetch_all($res, MYSQLI_ASSOC);
    }

    function modalNewProdotto() {
        $content = "<div class='modal-content'>"
                . "<h4 class='center'>Nuovo Prodotto</h4><br>";
        $content .= '<div class="row"><div class="input-field col s6 offset-s3"><input type="text" id="nomenewextra"><label for="nomenewextra">Nome</label></div>';
        $content .= "</div>";
        $content .= "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='confermanuovoprodotto' value=''>Crea</a></div></div>";
        //$content .= "</div>";
        return $content;
    }

    function newProdottoDuplicate($nome) {
        $db_query = "SELECT * FROM extra WHERE nome = '$nome'";
        $res = wrap_db_query($db_query);
        if (mysqli_num_rows($res) > 0) {
            return true;
        } else {
            return false;
        }
    }

    //controllo nome duplicato
    function newProdotto($nome) {
        $check = true;
        $db_query = "INSERT INTO extra (nome, stato) VALUES ('$nome', 0)";
        $res = wrap_db_query($db_query);
        $db_query = "SELECT id FROM extra WHERE nome = '$nome'";
        $res = wrap_db_query($db_query);
        $id = mysqli_fetch_assoc($res);
        foreach ($this->getIdTariffe() as $x) {
            $db_query = "INSERT INTO extra_" . $x['id'] . " (id) VALUES (" . $id['id'] . ")";
            $res = wrap_db_query($db_query);
        }
        return "end";
    }

    function modificaExtra($id, $arr) {
        $ids = array_shift($arr);
        $db_query = "DROP TABLE extra_" . $id;
        $res = wrap_db_query($db_query);
        $db_query = "CREATE TABLE extra_" . $id . " ( ";
        $i = 0;
        $len = count($ids);
        foreach ($ids as $x) {
            if ($i == 0) {
                $db_query .= "`$x` INT(11) PRIMARY KEY";
            } else {
                $db_query .= "`$x` INT(11) DEFAULT NULL";
            }
            if ($i != $len - 1) {
                $db_query .= ", ";
            }
            $i++;
        }
        $db_query .= "); ";
        $res = wrap_db_query($db_query);
        foreach ($arr as $x) {
            $db_query = "INSERT INTO extra_" . $id . " VALUES (";
            $i = 0;
            $len = count($x);
            foreach ($x as $y) {
                if ($y == "") {
                    $db_query .= "NULL";
                } else {
                    $db_query .= "'" . $y . "'";
                }
                if ($i != $len - 1) {
                    $db_query .= ", ";
                }
                $i++;
            }
            $db_query .= " ) ";
            $res = wrap_db_query($db_query);
        }
    }

    /*
     * PERIODO
     */

    function modalNewPeriodo($campo) {
        $content = "<div class='modal-content'>"
                . "<h4 class='center'>Nuovo Periodo</h4><br>";
        $content .= '<div class="row"><div class="input-field col s6 offset-s3"><input type="date" class="datepicker" id="datanewperiodo"><label for="nomenewextra">Data</label></div>';
        $content .= "</div>";
        $content .= "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='newperiodo' value='$campo'>Crea</a></div></div>";
        //$content .= "</div>";
        return $content;
    }

    function newPeriodo($campo, $data) {
        $arr = explode("/", $data);
        $db_query = "SELECT MAX(id_periodo) FROM periodi WHERE id_campo = '$campo'";
        $res = wrap_db_query($db_query);
        $return = mysqli_fetch_row($res);
        $idperiodo = $return[0] + 1;
        $db_query = "INSERT INTO periodi (`id_campo`, `id_periodo`, `inizio_tariffa`, `extra` ) VALUES"
                . " ($campo, $idperiodo, '$arr[1]/$arr[0]', '0')";
        $res = wrap_db_query($db_query);
        $db_query = "CALL newcampoorario('cal_camp" . $campo . "_pe" . $idperiodo . "')";
        $res = wrap_db_query($db_query);
    }

    /**
     * PAGINA UTENTI
     */
    function utentiPage() {
        $this->setUtenti();
        $content = "<div class='container'>"
                . "<div class='row'>"
                . "<div class='col s12'>"
                . "<ul class='tabs tabs-fixed-width'>"
                . "<li class='tab'><a class='active' href='#tabutenti'>Tabella Utenti</a></li>"
                . "<li class='tab'><a href='#prenotazioni'>Prenotazioni</a></li>"
                . "<li class='tab'><a href='#tipologie'>Tipologie</a></li>"
                . "</ul>"
                . "</div>"
                . "<div id='tabutenti' class='col s12'>" . $this->getTabellaUtenti() . "</div>"
                . "<div id='prenotazioni' class='col s12'>" . $this->getTabellaPrenotazioni() . "</div>"
                . "<div id='tipologie' class='col s12'>" . $this->getTabTipologie() . "</div>"
                . "</div>"
                . "</div>";
        return $content;
    }

    private function setUtenti() {
        $db_query = "SELECT username, email, unome, ucognome, udata, udatareg, utelefono FROM users, anagrafica WHERE users.uid = anagrafica.uid";
        $res = wrap_db_query($db_query);
        $this->utenti = mysqli_fetch_all($res, MYSQLI_ASSOC);
    }

    private function getTabellaUtenti() {
        $content = ""
                . "<table class='striped' id='utentitabella'>"
                . "<thead>"
                . "<th> Username </th>"
                . "<th> Email </th>"
                . "<th> Nome Cognome </th>"
                . "<th> Data nascita </th>"
                . "<th> Data Registrazione </th>"
                . "<th> Numero telefono </th>"
                . "</thead>"
                . "<tbody>";
        foreach ($this->utenti as $x) {
            $content .= "<tr> <td> " . $x['username'] . " </td> <td> " . $x['email'] . " </td> <td>  " . $x['ucognome'] . " " . $x['unome'] . "  </td> <td>  " . $x['udata'] . "  </td> <td>  " . $x['udatareg'] . "  </td>  <td> " . $x['utelefono'] . " </td> </tr>";
        }
        $content .= "</tbody>"
                . "</table>"
                . "";

        return $content;
    }

    private function setPrenotazioni() {
        $db_query = "SELECT * FROM prenotazioni";
        $res = wrap_db_query($db_query);
        $this->prenotazioni = mysqli_fetch_all($res, MYSQLI_ASSOC);
    }

    private function getNiceDate($anno, $mese, $giorno) {
        $date = new DateTime($anno . "-" . $mese . "-" . $giorno);
        return $date->format('d/m/y');
    }

    private function getNiceHour($time) {
        $arr = explode("/", $time);
        $h = $arr[0];
        $m = $arr[1];
        $date = new DateTime($h . ":" . $m);
        $content = $date->format('H:i');
        $content .= " alle ";
        $date->modify('+30 minute');
        $content .= $date->format('H:i');
        return $content;
    }

    private function getTabellaPrenotazioni() {
        $this->setPrenotazioni();
        $content = '<table class="striped" id="prenotazionitab">'
                . '<thead>'
                . '<th> Chi </th>'
                . '<th> Data </th>'
                . '<th> Giorno </th>'
                . '<th> Orario </th>'
                . '<th> Campo </th>'
                . '<th> Opzioni </th>'
                . '<th> Pagato </th>'
                . '</thead>'
                . '<tbody>';
        foreach ($this->prenotazioni as $x) {
            $content .= $this->getRow($x);
        }
        $content .= "</tbody> </table>";
        return $content;
    }

    private function getRow($x) {
        return "<tr> <td> " . $x['reservation_user_name'] . " </td> <td> " . $x['reservation_made_time'] . " </td> <td> " . $this->getNiceDate($x['reservation_year'], $x['reservation_week'], $x['reservation_day']) . " </td> <td> " . $this->getNiceHour($x['reservation_time']) . " </td> <td> " . $this->getNomeCampo($x['reservation_campo']) . " </td> <td> " . $this->getProdotto($x['reservation_prod']) . " </td> <td> " . $this->getPagato($x['reservation_pagato']) . " </td> </tr>";
    }

    private function getProdotto($id) {
        $this->setProdotti();
        $content = "";
        $arr = explode(",", $id);
        $int = 0;
        if ($id == null) {
            return "Nessuno";
        }
        foreach ($arr as $x) {
            foreach ($this->prodotti as $k) {
                if ($x == $k['id']) {
                    if ($int > 0) {
                        $content .= ", " . $k['nome'];
                    } else {
                        $content .= $k['nome'];
                        $int++;
                    }
                }
            }
        }
        return $content;
    }

    private function getPagato($a) {
        if ($a == 0) {
            return "No";
        } else {
            return "Si";
        }
    }

    private function getNomeCampo($id) {
        $db_query = "SELECT cnome FROM campo WHERE cid = $id";
        $res = wrap_db_query($db_query);
        $rr = mysqli_fetch_assoc($res);
        return $rr['cnome'];
    }

    /**
     * Tabelle tipologie utenti
     */
    private function getTabTipologie() {
        $query = "SELECT * FROM tipologia";
        $res = wrap_db_query($query);
        $result = mysqli_fetch_all($res);
        $content = "<table class='centered striped'>"
                . "<thead>"
                . "<th> Nome </th>"
                . "</thead>"
                . "<tbody>";
        $i = 0;
        $len = count($result);
        foreach ($result as $x) {
            if ($i != $len - 1) {
                $content .= "<tr value='$x[0]'>"
                        . "<td class='setted'> $x[1] </td>"
                        . "</tr>";
            }
            $i++;
        }
        $content .= "</tbody> </table>"
                . "<a class='btn waves-effect waves-light purple' id='newsocio'>Aggiungi nuovo</a>"
                . "</div>";
        return $content;
    }

    function newSocioModal() {
        $content = "<div class='modal-content'>"
                . "<h4 class='center'> Nuovo socio </h4>"
                . "<div class='row'><div class='input-field col s4 offset-s4'><input type='text' id='nomenewsocio'>"
                . "<label for='nomenewsocio'>Nome</label>"
                . "</div></div>"
                . "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnnewsocio' value=''>Crea</a></div></div>";
        return $content;
    }

    function newSocio($socio) {
        $db_query = "SELECT tnome FROM tipologia WHERE tnome = '$socio'";
        $res = wrap_db_query($db_query);

        if (mysqli_num_rows($res) > 0) {
            return "Duplicato";
        } else {
            $db_query = "SELECT * FROM tipologia";
            $result = wrap_db_query($db_query);
            $arr = mysqli_fetch_all($result, MYSQLI_ASSOC);
            $db_query = "TRUNCATE TABLE tipologia";
            $res = wrap_db_query($db_query);
            $i = 0;
            $len = count($arr);
            foreach ($arr as $x) {
                if ($i == $len - 1) {
                    $db_query = "INSERT INTO tipologia (`tid`, `tnome`) VALUES ($i, '$socio')";
                    $res = wrap_db_query($db_query);
                    $db_query = "INSERT INTO tipologia (`tid`, `tnome`) VALUES (1000, 'Guest')";
                    $res = wrap_db_query($db_query);
                } else {
                    $db_query = "INSERT INTO tipologia (`tid`, `tnome`) VALUES (" . $x['tid'] . ", '" . $x['tnome'] . "')";
                    $res = wrap_db_query($db_query);
                }
                $i++;
            }
            $db_query = "SELECT tid FROM tipologia WHERE tnome = '$socio'";
            $res = wrap_db_query($db_query);
            $aa = mysqli_fetch_assoc($res);
            $idnewsocio = $aa['tid'];

            $db_query = "SELECT id FROM tariffe";
            $result = wrap_db_query($db_query);
            $arr2 = mysqli_fetch_all($result, MYSQLI_ASSOC);
            foreach ($arr2 as $y) {
                $db_query = "ALTER TABLE tariffe_" . $y['id'] . " ADD COLUMN `" . $idnewsocio . "` INT(11) DEFAULT NULL";
                $res = wrap_db_query($db_query);
                $db_query = "ALTER TABLE extra_" . $y['id'] . " ADD COLUMN `" . $idnewsocio . "` INT(11) DEFAULT NULL";
                $res = wrap_db_query($db_query);
            }
        }
    }

}
