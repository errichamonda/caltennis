<?php

class ModalPrenota {

    private $campo = array();
    private $prodotti = array();
    private $attivita;
    private $id;
    private $max;
    private $min;
    private $periodi;
    private $tariffa;
    private $tariffatab;
    /**
     * Costruttore
     */
    public function __construct() {
        include "../includes/database.php";
        setlocale(LC_TIME, 'ita', 'it_IT');
        $conn = wrap_db_connect();
        include "attivita.php";
        $this->attivita = new Attivita($conn);
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $this->setProdotti();
        $this->getMaxMinOra();
        $this->setPeriodi();
    }

    private function setPeriodi() {
        $db_query = "SELECT * FROM tariffe_date";
        $res = wrap_db_query($db_query);
        $this->periodi = mysqli_fetch_all($res);
    }

    private function getMaxMinOra() {
        $count = 0;
        $db_query = "SELECT CONCAT('SELECT MAX(ora) AS max, MIN(ora) AS min FROM ', table_name, ' WHERE `0` IS NOT NULL OR `1` IS NOT NULL"
                . " OR `2` IS NOT NULL OR `3` != NULL OR `4` IS NOT NULL OR `5` IS NOT NULL OR `6` IS NOT NULL ')"
                . " AS statement FROM information_schema.tables WHERE table_name LIKE 'cal_camp%' GROUP BY table_name";
        $result = wrap_db_query($db_query);
        $p = mysqli_fetch_all($result);
        $query = "SELECT MIN(min) as mins, MAX(max) as maxs FROM ( ";
        foreach ($p as $x) {
            //return $x[0];
            $query .= $x[0];
            if ($count != count($p) - 1) {
                $query .= "\n UNION \n";
            } else {
                $query .= " ) AS temp";
            }
            $count++;
        }
        $res = wrap_db_query($query);
        $dud = mysqli_fetch_assoc($res);
        $this->max = $dud["maxs"];
        $this->min = $dud["mins"];
    }

    function setCampo($id) {
        $query = "SELECT * FROM campo WHERE cid = $id";
        $res = wrap_db_query($query);
        $result = mysqli_fetch_assoc($res);
        $this->campo = $result;
    }

    function isEvento($year, $week, $day, $time, $id) {
        $query = "SELECT * FROM prenotazioni WHERE reservation_year = '$year' AND reservation_week = '$week' AND reservation_day = '$day' AND reservation_time = '$time' AND reservation_campo = '$id' ";
        $res = wrap_db_query($query);
        $result = mysqli_fetch_all($res);
        if ($result != null) {
            return true;
        } else {
            return false;
        }
    }

    function getCampo() {
        return $this->campo;
    }

    function setProdotti() {
        $query = "SELECT * FROM extra";
        $res = wrap_db_query($query);
        $this->prodotti = mysqli_fetch_all($res);
    }

    function getCampoName() {
        return $this->campo['cnome'];
    }

    function getNiceDate($anno, $mese, $giorno, $h, $m) {
        $date = new DateTime($anno . "-" . $mese . "-" . $giorno . " " . $h . ":" . $m);
        $content = "<p class='center'>" . strftime('%a %d %B %Y', $date->getTimestamp());
        $content .= "<br>" . $date->format('H:i') . " a ";
        $date->modify('+30 minute');
        $content .= "" . $date->format('H:i') . "</p>";
        return $content;
    }

    function checkReservation($year, $week, $day, $time, $campo) {
        $query = "SELECT * FROM prenotazioni WHERE reservation_year = '$year' AND reservation_week = '$week' AND reservation_day = '$day' AND reservation_time = '$time' AND reservation_campo = '$campo' ";
        $res = wrap_db_query($query);
        $result = mysqli_fetch_all($res);
        if ($result != null) {
            $count = $this->attivita->getMaxUserById($result[0][12]);
            $int = $count - count($result);
            if (count($result) >= $count) {
                return false;
            } else {
                // TODO: check se può iscriversi / prenotare
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     * Controllo data per il periodo
     * @param type $id
     * @param type $data
     * @return boolean
     */
    function getPeriodoCampo($id, $data) {
        $checkDate = new DateTime($data);
        foreach ($this->periodi as $x) {
            if ($x[1] == $id) {
                $lowerBound = new DateTime($x[3]);
                $upperBound = new DateTime($x[4]);
                if ($lowerBound < $upperBound) {
                    if ($lowerBound <= $checkDate && $checkDate <= $upperBound) {
                        return $x[2];
                    }
                } else {
                    if ($checkDate <= $upperBound || $checkDate >= $lowerBound) {
                        return $x[2];
                    }
                }
            }
        }
        return false;
    }

    function canSub($tariffa) {
        $check = false;
        $arr = $this->getTariffa($tariffa);
        $privi = $this->getPrivileges();
        if ($privi != "guest") {
            foreach ($arr as $x) {
                if ($x["$privi"]) {
                    $check = true;
                }
            }
        }
        return $check;
    }
    
    public function setTariffa($day){
        
    }

    function getTabPrezzo() {
        //TODO TABELLA PREZZI / ATTIVITA
        $content = "<table class='striped'>"
                . "<tbody>";
        $privi = $this->getPrivileges();
        $arr = array();
        foreach($this->tariffatab as $x){
            if($x["$privi"] != NULL){
                $content .= "<tr><td> <input type='radio' name='attivita' class='filled-in attivita' id='attivita".$x['id']."'> "
                        . "<label for='attivita".$x['id']."'>".$this->attivita->getAttivitaById($x['id'])."</label></td>"
                        . "<td>".$x[0] /100 ." €</td></tr>";
            }
        }
        $content .= "</tbody></table>";
        return $content;
    }

    function getProdotto($id) {
        foreach ($this->prodotti as $x) {
            if ($x[0] == $id) {
                return $x;
            }
        }
    }

    function getIntProdotti() {
        //TODO getProdotti e shit
    }

    function getProdotti() {
        $prodotti = explode(",", $this->campo['oid']);
        $content = "<table class='striped'> <tbody>";
        foreach ($prodotti as $x) {
            $prod = $this->getProdotto($x);
            if ($prod[2] == 1) {
                $prezzo = $prod[3] / 100 . " €";
                /* $content .= "<div class='row'>"
                  . "<div class='left-align'>"
                  . "<input type='checkbox' class='filled-in prodotto' id='prodotto" . $prod[0] . "' value='".$prod[0]."' />"
                  . "<label for='prodotto" . $prod[0] . "'> $prod[1] </label>"
                  . "</div>"
                  . "<div class='right-align'> $prezzo </div>"
                  . "</div>"; */
                $content .= "<tr>"
                        . "<td> <input type='checkbox' class='filled-in prodotto' id='prodotto" . $prod[0] . "' value='" . $prod[0] . "' />"
                        . "<label for='prodotto" . $prod[0] . "'> $prod[1] </label> </td>"
                        . "<td> $prezzo </td>"
                        . "</tr>";
            }
        }
        $content .= "</tbody></table>";
        return $content;
    }

    function getFooter($val) {
        $content = "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnmodprenota' value='" . $val . "'>Prenota</a></div>";
        return $content;
    }

    function getFooterEvento($val) {
        $val .= "/" . $this->id;
        $content = "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnmodprenotaev' value='" . $val . "'>Prenota</a></div>";
        return $content;
    }

    function setPrenota($anno, $mese, $giorno, $ora, $campo, $prod, $user, $evento) {
        $query = "INSERT INTO prenotazioni (reservation_made_time, reservation_year, reservation_week, reservation_day, reservation_time, reservation_campo, reservation_user_id, reservation_user_name, reservation_prod, reservation_attivita) "
                . "VALUES (now(), $anno, $mese, $giorno, '$ora', $campo, 0, '$user', '$prod', '$evento')";

        $res = wrap_db_query($query);
        if ($res) {
            return "Prenotato";
        } else {
            return "Errore";
        }
    }

    function setPrenotaEvento($anno, $mese, $giorno, $ora, $campo, $prod, $user, $evento) {
        $query = "INSERT INTO prenotazioni (reservation_made_time, reservation_year, reservation_week, reservation_day, reservation_time, reservation_campo, reservation_user_id, reservation_user_name, reservation_prod, reservation_attivita) "
                . "VALUES (now(), $anno, $mese, $giorno, '$ora', $campo, 0, '$user', '$prod', '$evento')";

        $res = wrap_db_query($query);
        if ($res) {
            return "Prenotato";
        } else {
            return "Errore";
        }
    }

    function getNomeAttivita() {
        return $this->attivita->getAttivitaById($this->id);
    }

    function getAutoFill() {
        /* $arr = explode("/", $this->attivita->canSub($this->campo['cid']));
          $content = "";
          foreach ($arr as $x) {
          $content .= "<p> <input name='tipo' type='radio' id='radio$x' value='$x' /> <label for='radio$x'>" . $this->attivita->getAttivitaById($x) . "</label></p>";
          }
          return $content; */
    }

    function isWalkable($year, $week, $day, $time, $campo) {
        $tim = explode("/", $time);
        $uDCheck = strtotime($week . "/" . $day . "/" . $year);
        $uHCheck = strtotime($tim[0] . ":" . $tim[1] . ":00");
        $tOra = date_create();
        $uDOra = strtotime(date_format($tOra, 'm/d/Y'));
        $uHOra = strtotime(date_format($tOra, 'H:i:s'));
        $this->tariffa = $this->getPeriodoCampo($campo, "$week/$day");
        if (!$this->tariffa) {
            return false;
        } else {
            if ($uDCheck < $uDOra) {
                return false;
            } else {
                if ($uDCheck == $uDOra) {
                    if ($uHCheck < $uHOra) {
                        return false;
                    } else {
                        if ($this->loadCalendarioCampo($campo, $uDCheck, $uHCheck, $this->tariffa)) {
                            if ($this->canSub($this->tariffa)) {
                                return $this->checkReservation($year, $week, $day, $time, $campo);
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    }
                } else {
                    if ($this->loadCalendarioCampo($campo, $uDCheck, $uHCheck, $this->tariffa)) {
                        if ($this->canSub($this->tariffa)) {
                            return $this->checkReservation($year, $week, $day, $time, $campo);
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            }
        }
    }

    /**
     * Controlla se quel slot è attivo oppure no
     * @param type $campo
     * @param type $data
     * @param type $ora
     * @param type $periodo
     * @return boolean
     */
    function loadCalendarioCampo($campo, $data, $ora, $periodo) {
        $weekday = date("w", $data);
        $arr = array();
        foreach ($this->setCalendario($campo, $periodo) as $x) {
            $checkTime = strtotime($x['ora']);
            if ($ora == $checkTime) {
                if ($x["$weekday"]) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    function setCalendario($id, $periodo) {
        $db_query = "SELECT * FROM cal_camp$id" . "_pe$periodo";
        $res = wrap_db_query($db_query);
        $ret = mysqli_fetch_all($res, MYSQLI_ASSOC);
        return $ret;
    }

    function canWalk($campo) {
        $query = "SELECT tid FROM campo WHERE cid = $campo";
        $res = wrap_db_query($query);
        $result = wrap_db_fetch_array($res);
        $arr = explode(",", $result['tid']);
        if (in_array($this->getPrivileges(), $arr)) {
            return true;
        } else {
            return false;
        }
    }

    function getPrivileges() {
        $query = "SELECT tid FROM anagrafica, users WHERE username = '" . $_SESSION['user'] . "' AND anagrafica.uid = users.uid";
        $res = wrap_db_query($query);
        $result = wrap_db_fetch_array($res);
        return "" . $result['tid'];
    }
    
    private function getTariffa($id) {
        $db_query = "SELECT * FROM tariffe_$id";
        $res = wrap_db_query($db_query);
        $ret = mysqli_fetch_all($res, MYSQLI_ASSOC);
        $this->tariffatab = $ret;
        return $ret;
    }

}
