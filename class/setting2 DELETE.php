<?php
//USELESS
class Setting {

    private $prodotti;
    private $campi;
    private $soci;
    private $attivita;
    private $attivitacampo;

    /**
     * Costruttore
     */
    public function __construct() {
        include "../includes/database.php";
        setlocale(LC_TIME, 'ita', 'it_IT');
        $conn = wrap_db_connect();
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
    }

    function setAttivita() {
        $db_query = "SELECT * FROM attivita";
        $result = wrap_db_query($db_query);
        $this->attivita = mysqli_fetch_all($result);
    }

    function setAttivitaCampo($id) {
        $db_query = "SELECT attivita FROM campo WHERE cid = '$id'";
        $result = wrap_db_query($db_query);
        $this->attivitacampo = mysqli_fetch_all($result);
    }

    function setProdotti() {
        $db_query = "SELECT * FROM opzioni";
        $res = wrap_db_query($db_query);
        $this->prodotti = mysqli_fetch_all($res);
    }

    function setSoci() {
        $db_query = "SELECT * FROM tipologia";
        $res = wrap_db_query($db_query);
        $this->soci = mysqli_fetch_all($res);
    }

    function getProd() {
        return $this->prodotti;
    }

    /**
     * inutilizzata
     * @return string
     */
    function getSetting() {
        $content = "<div class='container'>"
                . "<ul class='collapsible' data-collapsible='expandable'>";
        $content .= $this->getCampi();
        $content .= $this->getGeneral();
        $content .= $this->getProdotti();
        $content .= $this->getUtenti();
        $content .= $this->getPrenotazioni();
        $content .= "</ul></div>";
        return $content;
    }

    function setCampi() {
        $res = wrap_db_query("SELECT * FROM campo");
        $this->campi = mysqli_fetch_all($res);
    }

    function getUtenti() {
        $res = wrap_db_query("SELECT * FROM anagrafica");
        $result = mysqli_fetch_all($res);
        $content = ''
                . '<li>'
                . '<div class="collapsible-header"><i class="material-icons">person</i> Utenti </div>'
                . '<div class="collapsible-body">'
                . '<table class="striped">'
                . '<thead> <tr> <th>Nome Cognome</th><th>Data Nascita</th><th>Data Iscrizione</th><th>Telefono</th><th>email</th><th>Tipologia</th></tr></thead>'
                . '<tbody>';
        foreach ($result as $x) {
            $content .= '<tr id="utent' . $x[0] . '">'
                    . '<td>'
                    . $x[1] . ' ' . $x[2]
                    . '</td><td>'
                    . $x[3]
                    . '</td><td>'
                    . $x[4]
                    . '</td><td>'
                    . $x[5]
                    . '</td><td>'
                    . $x[6]
                    . '</td><td>'
                    . $x[7] . '</td></tr>';
        }
        $content .= '</tbody> </table> </div> </li>';
        return $content;
    }

    function getGeneral() {
        $res = wrap_db_query("SELECT * FROM impostazioni");
        $result = mysqli_fetch_all($res);
        $content = "<div class='container'>"/* '<li> <div class="collapsible-header"><i class="material-icons">settings</i>Generali</div>'
                  . '<div class="collapsible-body">' */
                . '<table> <thead> <tr> <th> Chiave </th> <th> Valore </th> </tr> </thead> <tbody>';
        foreach ($result as $x) {
            $content .= '<tr> <td>'
                    . $x[1]
                    . '</td> <td>' . $x[2] . '</td>'
                    . '</tr>';
        }
        $content .= '</tbody></table></div>';
        return $content;
    }

    function newCampoModal() {
        $content = //"<div id='modalprodotti' class='modal'>"
                "<div class='modal-content'>"
                . "<h4 class='center'>Nuovo campo</h4>";
        $content .= '<div class="row"><div class="input-field col s6">Nome <input type="text" id="nomenewcampo"></div>';
        $content .= '<div class="input-field col s6">Prezzo <input type="number" id="prezzonewcampo"></div></div>';
        $content .= '<div class="row"><div class="input-field col s6">Apertura <input class="timepicki" id="inizionewcampo"></div>';
        $content .= '<div class="input-field col s6">Chiusura <input class="timepicki" id="finenewcampo"></div></div><br><p class="center">I permessi vanno settati successivamente</p><br>';

        $content .= "</div>";
        $content .= "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnnewcampo' value=''>Imposta</a></div></div>";
        //$content .= "</div>";
        return $content;
    }

    function getCampi() {
        $this->setProdotti();
        $result = $this->campi;
        $content = "<div class='container'>"
                . '<table class="striped tablecampi centered">'
                . '<thead> <tr> <th>NomeCampo</th><th>Stato</th><th class="orario">Orario Apertura</th><th class="orario">Orario Chiusura</th><th class="orario">Prezzo €</th><th>Extra</th><th>Permessi</th><th>Attività</th><th>Giorni</th><th class="center">Modifica</th></tr></thead>'
                . '<tbody>';
        if ($result == null) {
            $content .= "<tr><td colspan='9'>Nessun campo trovato</td></tr>";
        } else {
            foreach ($result as $x) {
                $content .= '<tr value="' . $x[0] . '"> <td class="setted">' . $x[1] . '</td>'
                        . '<td> <div class="input-field sel"> <select>';
                if ($x[3] == 1) {
                    $content .= "<option value='1' selected>Aperto</option>"
                            . "<option value='0'>Chiuso</option>";
                } else {
                    $content .= "<option value='1'>Aperto</option>"
                            . "<option value='0' selected>Chiuso</option>";
                }
                $inizio = date_create_from_format('H:i:s', $x[4]);
                $fine = date_create_from_format('H:i:s', $x[5]);
                $content .= "</select></div></td><td class='ore'> <input type='text' class='timepicki' value='" . $inizio->format("H:i") . "' data-timepicki-tim='" . $inizio->format("H") . "' data-timepicki-mini='" . $inizio->format("i") . "'> </input> </td><td class='ore'> <input class='timepickf'  value='" . $fine->format("H:i") . "' data-timepicki-tim='" . $fine->format("H") . "' data-timepicki-mini='" . $fine->format("i") . "'> </input>   </td> <td class='setted'>";
                $prezzo = $x[2] / 100;
                $content .= "$prezzo</td>"
                        . "<td> <a class='btn waves-effect waves-light orange settprod center-text' value='" . $x[6] . "'>Extra</a></td>"
                        . "<td> <a class='btn waves-effect waves-light orange settsoci center-text' value='" . $x[7] . "'>Permessi</a></td>"
                        . "<td> <a class='btn waves-effect waves-light orange settatt center-text' value='" . $x[8] . "'>Attivita</a></td>"
                        . "<td> <a class='btn waves-effect waves-light orange settgiorni center-text' value='" . $x[8] . "'>Giorni</a></td>"
                        . " <td class='center'> <a class='btn waves-effect waves-light btn-flat settbtn center-text' value='" . $x[0] . "'>Modifica</td>";
                $content .= "</tr>";
            }
        }
        $content .= '</tbody> </table>'
                . '<a class="btn waves-effect waves-light orange" id="newcampo">Aggiungi nuovo </a> </div>';
        return $content;
    }

    function getModalGiorni($id) {
        $this->setProdotti();
        $content = "<div class='modal-content'>"
                . "<h4>Giorni</h4>"
                . "<div class='col s4'>"
                . "<table class='striped'>"
                . "<tbody>";
        $timestamp = strtotime('next Sunday');
        for ($i = 0; $i < 7; $i++) {
            $content .= "<tr><td>"
                    . "<div class='left-align'>"
                    . "<input type='checkbox' class='filled-in giorno' id='giorno$i' value='$i' ". $this->isGiornoOn($id, $i) . "' />"
                    . "<label for='giorno$i'>".date('l', $timestamp)."</label>"
                    . "</div>"
                    . "</td></tr>";
            $timestamp = strtotime('+1 day', $timestamp);
        }
        $content .= "</tbody></table></div>";
        $content .= "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnmodextra' value=''>Imposta</a></div></div>";
        //$content .= "</div>";
        return $content;
    }
    
    function isGiornoOn($idC, $idG){
        $this->setCampi();
        foreach($this->campi as $x){
            if($x[0] == $idC){
                $arr = explode(",", $x[9]);
                if(in_array($idG, $arr)){
                    return "checked = 'true'";
                } else {
                    return "";
                }
            }
        }
    }

    function isProdOn($idC, $idP) {
        $asd = $this->campi;
        if ($idP == "") {
            return "";
        } else {
            foreach ($asd as $x) {
                if ($idC == $x[0]) {
                    $prod = explode(",", $x[6]);
                    foreach ($prod as $y) {
                        if ($idP == $y) {
                            return "checked='true' ";
                        }
                    }
                    return "";
                }
            }
        }
    }

    function modalProdotti($idC) {
        $content = "<div class='modal-content'>"
                . "<h4>Prodotti</h4>"
                . "<div class='col s4 '>"
                . "<table class='striped'>"
                . "<tbody>";
        foreach ($this->prodotti as $x) {
            $content .= "<tr><td>"
                    . "<div class='left-align'>"
                    . "<input type='checkbox' class='filled-in prodotto' id='prodotto" . $x[0] . "' value='" . $x[0] . "' " . $this->isProdOn($idC, $x[0]) . "/>"
                    . "<label for='prodotto" . $x[0] . "'> $x[1]</label></div></td></tr>";
        }
        $content .= "</tbody></table></div>";
        $content .= "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnmodextra' value=''>Imposta</a></div></div>";
        //$content .= "</div>";
        return $content;
    }

    function modalAttivita($idC) {
        $content = "<div class='modal-content'>"
                . "<h4 class='center'>Attività</h4>"
                . "<p class='center'>Deve essere presente almeno una attività</p>"
                . "<table class='striped'>"
                . "<tbody>";
        ;
        foreach ($this->attivita as $x) {
            $content .= "<tr><td>"
                    . "<div class='left-align'><input type='checkbox' class='filled-in attimod' id='att" . $x[0] . "' value='" . $x[0] . "' " . $this->isAttOn($idC, $x[0]) . "/>"
                    . "<label for='att" . $x[0] . "'> $x[1]</label></td></td></tr>";
        }
        $content .= "</tbody>";
        $content .= "</table>";
        $content .= "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnmodatt' value=''>Imposta</a></div></div>";
        //$content .= "</div>";
        return $content;
    }

    function isAttOn($idC, $idA) {
        $asd = $this->campi;
        foreach ($asd as $x) {
            if ($idC == $x[0]) {
                $att = explode(",", $x[8]);
                foreach ($att as $y) {
                    if ($idA == $y) {
                        return "checked='true' ";
                    }
                }
                return "";
            }
        }
    }

    function isSocioIn($idC, $idS) {
        $asd = $this->campi;
        if ($idS == 0) {
            return "checked='checked' disabled='disabled' ";
        } else {
            foreach ($asd as $x) {
                if ($idC == $x[0]) {
                    $prod = explode(",", $x[7]);
                    foreach ($prod as $y) {
                        if ($idS == $y) {
                            return "checked='true' ";
                        }
                    }
                    return "";
                }
            }
        }
    }

    function modalChiprenota($idC) {
        $content = "<div class='modal-content'>"
                . "<h4 class='center'>Soci</h4>"
                . "<p class='center'>L'amministratore ha accesso a tutti i campi</p>"
                . "<table class='striped'>"
                . "<tbody>";

        foreach ($this->soci as $x) {
            $content .= "<tr><td>"
                    . "<div class='left-align'><input type='checkbox' class='filled-in socimod' id='soci" . $x[0] . "' value='" . $x[0] . "' " . $this->isSocioIn($idC, $x[0]) . "/>"
                    . "<label for='soci" . $x[0] . "'> $x[1]</label></td></td></tr>";
        }
        $content .= "</tbody>";
        $content .= "</table>";
        $content .= "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnmodsoci' value=''>Imposta</a></div></div>";
        //$content .= "</div>";
        return $content;
    }

    function getProdotti() {
        $res = wrap_db_query("SELECT * FROM opzioni");
        $result = mysqli_fetch_all($res);
        $content = "<div class='container'>"/* '<li>'
                  . '<div class="collapsible-header"><i class="material-icons">add</i>Prodotti extra</div>'
                  . '<div class="collapsible-body">' */
                . '<table class="striped centered">'
                . '<thead> <tr> <th>Nome Prodotto</th><th>Stato</th><th>Prezzo €</th><th>Modifica</th></tr></thead>'
                . '<tbody>';
        if ($result == null) {
            $content .= "<tr><td colspan='4'>Nessun prodotto trovato</td></tr>";
        } else {
            foreach ($result as $x) {
                $content .= '<tr> <td class="setted">' . $x[1] . '</td>'
                        . '<td> <div class="input-field sel"> <select>';
                if ($x[2] == 1) {
                    $content .= "<option value='1' selected>Abilitato</option>"
                            . "<option value='0'>Disabilitato</option>";
                } else {
                    $content .= "<option value='1'>Abilitato</option>"
                            . "<option value='0' selected>Disabilitato</option>";
                }
                $content .= "</select></div></td><td class='setted'>";
                $prezzo = $x[3] / 100;
                $content .= "$prezzo</td> <td class='center'> <a class='btn waves-effect waves-light btn-flat extramod center-text' value='" . $x[0] . "'>Modifica</a></td> </td>";
                $content .= "</tr>";
            }
        }
        $content .= '</tbody> </table>'
                . '<a class="btn waves-effect waves-light" id="newextra">Aggiungi nuovo</a>'
                . '</div>';
        return $content;
    }

    function getSoci() {
        $query = "SELECT * FROM tipologia";
        $res = wrap_db_query($query);
        $result = mysqli_fetch_all($res);
        $content = "<div class='container'>"
                . "<p><h5 class='center'>Soci</h5></p>"
                . "<table class='centered striped'>"
                . "<thead>"
                . "<th> Nome </th>"
                . "</thead>"
                . "<tbody>";
        foreach ($result as $x) {
            $content .= "<tr value='$x[0]'>"
                    . "<td class='setted'> $x[1] </td>"
                    . "</tr>";
        }
        $content .= "</tbody> </table>"
                . "<a class='btn waves-effect waves-light purple' id='newsocio'>Aggiungi nuovo</a>"
                . "</div>";
        return $content;
    }

    function newSocioModal() {
        $content = "<div class='modal-content'>"
                . "<h4 class='center'> Nuovo socio </h4>"
                . "<div class='row'><div class='input-field col s4 offset-s4'><input type='text' id='nomenewsocio'>"
                . "<label for='nomenewsocio'>Nome</label>"
                . "</div></div>"
                . "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnnewsocio' value=''>Crea</a></div></div>";
        return $content;
    }

    function getPrenotazioni() {
        return '<div class="container">'/* '<li>'
                  . '<div class="collapsible-header"><i class="material-icons">today</i>Prenotazioni</div>'
                  . '<div class="collapsible-body prenotabody">' */
                . '<p><h5 class="center">Quali prenotazioni mostrare?</h5><br>'
                . '<div class="row">'
                . '<a class="btn waves-effect waves-light blue col s2 offset-s1 prenotatab" id="prenotazionitutte"> Tutte </a>'
                //. '<a class="btn waves-effect waves-light blue col s2 offset-s2 prenotatab" id="prenotazionimonth"> Questo mese </a>'
                . '<a class="btn waves-effect waves-light blue col s2 offset-s2 prenotatab" id="prenotazioninotpaied"> Non pagate </a><br>'
                . '<div id="prenotatabella"></div>'
                . '</div>'
                . '</div>';
    }

    function newExtraModal() {
        $content = //"<div id='modalprodotti' class='modal'>"
                "<div class='modal-content'>"
                . "<h4 class='center'>Nuovo extra</h4>";
        $content .= '<div class="row"><div class="input-field col s6">Nome <input type="text" id="nomenewextra"></div>';
        $content .= '<div class="input-field col s6">Prezzo <input type="number" id="prezzonewextra"></div></div>';
        $content .= "</div>";
        $content .= "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnnewextra' value=''>Crea</a></div></div>";
        //$content .= "</div>";
        return $content;
    }

    function getPrivileges() {
        $query = "SELECT tid FROM anagrafica, users WHERE username = '" . $_SESSION['user'] . "' AND anagrafica.uid = users.uid";
        $res = wrap_db_query($query);
        $result = wrap_db_fetch_array($res);
        if ($result['tid'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    function getAttivita() {
        $content = "<div class='container'>"
                . "<p><h5 class='center'>Attività</h5></p>"
                . "<table class='centered striped'>"
                . "<thead>"
                . "<th> Nome attività </th> <th> Max Utenti </th> <th> Chi può creare </th> <th> Chi può iscriversi </th> <th> Modifica </th>"
                . "</thead>"
                . "<tbody>";
        foreach ($this->attivita as $x) {
            $content .= "<tr value='$x[0]'>"
                    . "<td class='setted'> $x[1] </td> <td class='setted'> $x[2] </a></td> <td><a class='btn btn-flat waves-effect attcreator' value='$x[3]'> CanCreate</a></td> <td><a class='btn btn-flat waves-effect attsub' value='$x[4]'>Paceholder</a></td>"
                    . "<td> <a class='btn btn-flat waves-effect waves-light modificaattivita'>Modifica</a></td>"
                    . "</tr>";
        }
        $content .= "</tbody> </table>"
                . "<a class='btn waves-effect waves-light purple' id='newattivita'>Aggiungi nuovo</a>"
                . "</div>";
        return $content;
    }

    function getModalAttivita() {
        $content = "<div class='modal-content'>"
                . "<h4 class='center'>Nuova attivita</h4>";
        $content .= '<div class="row"><div class="input-field col s6">Nome <input type="text" id="nomenewattivita"></div>';
        $content .= '<div class="input-field col s6">Max Utenti<input type="number" id="maxutentiattivita"></div></div>';
        $content .= "</div>";
        $content .= "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnnewattivita' value=''>Crea</a></div></div>";
        return $content;
    }

    function getModalAttivitaSub($id) {
        $content = "<div class='modal-content'>"
                . "<h4 class='center'>Soci</h4>"
                . "<p class='center'>L'amministratore ha accesso a tutte le attivita</p>"
                . "<table class='striped'>"
                . "<tbody>";

        foreach ($this->soci as $x) {
            $content .= "<tr><td>"
                    . "<div class='left-align'><input type='checkbox' class='filled-in socimod' id='soci" . $x[0] . "' value='" . $x[0] . "' " . $this->isSocioInAtt($id, $x[0]) . "/>"
                    . "<label for='soci" . $x[0] . "'> $x[1]</label></td></td></tr>";
        }
        $content .= "</tbody>";
        $content .= "</table>";
        $content .= "<div class='modal-footer'> <a class='modal-action waves-effect waves-green btn-flat' id='btnmodattsub' value=''>Imposta</a></div></div>";
        //$content .= "</div>";
        return $content;
    }

    function isSocioInAtt($idA, $idS) {
        if ($idS == 0) {
            return "checked='checked' disabled='disabled' ";
        } else {
            foreach ($this->attivita as $x) {
                if ($idA == $x[0]) {
                    $sub = explode(",", $x[4]);
                    foreach ($sub as $y) {
                        if ($idS == $y) {
                            return "checked='true' ";
                        }
                    }
                    return "";
                }
            }
        }
    }

}
