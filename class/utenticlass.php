<?php

class Utenti{
    
    private $utenti;
    
    public function __construct() {
        include "../includes/database.php";
        setlocale(LC_TIME, 'ita', 'it_IT');
        $conn = wrap_db_connect();
        $this->setUtenti();
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
    }
    
    private function setUtenti(){
        $db_query = "SELECT username, email, unome, ucognome, udata, udatareg, utelefono FROM users, anagrafica WHERE users.uid = anagrafica.uid";
        $res = wrap_db_query($db_query);
        $this->utenti = mysqli_fetch_all($res);
    }
    
    public function getUtenti(){
        $content = "<div class='container'>"
                . "<table class='striped' id='utentitabella'>"
                . "<thead>"
                . "<th> Username </th>"
                . "<th> Email </th>"
                . "<th> Nome Cognome </th>"
                . "<th> Data nascita </th>"
                . "<th> Data Registrazione </th>"
                . "<th> Numero telefono </th>"
                . "</thead>"
                . "<tbody>";
        foreach($this->utenti as $x){
            $content .= "<tr> <td> $x[0] </td> <td> $x[1] </td> <td> $x[2] $x[3] </td> <td> $x[4] </td> <td> $x[5] </td>  <td> $x[6] </td> </tr>";
        }
        $content .= "</tbody>"
                . "</table>"
                . "</div>";
        
        return $content;
    }
}
