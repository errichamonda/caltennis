<?php

class Calendario {

    private $campi = array();
    private $dayCalendar;
    private $max;
    private $min;
    private $periodi;
    private $priv;
    private $tariffaid;

    /**
     * Costruttore
     */
    public function __construct() {
        include "../includes/database.php";
        include "../includes/declares.php";
        setlocale(LC_TIME, 'ita', 'it_IT');
        $conn = wrap_db_connect();
        include "attivita.php";
        $this->attivita = new Attivita($conn);
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $this->setCampi();
        $this->setDayCalendar();
        $this->getMaxMinOra();
        $this->setPeriodi();
        $this->setPrivilegio();
    }

    /*
     * GET E SET
     */

    private function getTariffa($id) {
        $db_query = "SELECT * FROM tariffe_$id";
        $res = wrap_db_query($db_query);
        $ret = mysqli_fetch_all($res, MYSQLI_ASSOC);
        return $ret;
    }

    private function getColoreTariffa($id) {
        switch ($id) {
            case ("1"): return "blue";
                break;
            case ("2"): return "purple";
                break;
        }
    }

    private function setPrivilegio() {
        if (isset($_SESSION['user'])) {
            $query = "SELECT tid FROM anagrafica, users WHERE username = '" . $_SESSION['user'] . "' AND anagrafica.uid = users.uid";
            $res = wrap_db_query($query);
            $result = wrap_db_fetch_array($res);
            $this->priv = $result['tid'];
        } else {
            $this->priv = "guest";
        }
    }

    function setCalendario($id, $periodo) {
        $db_query = "SELECT * FROM cal_camp$id" . "_pe$periodo";
        $res = wrap_db_query($db_query);
        $ret = mysqli_fetch_all($res, MYSQLI_ASSOC);
        return $ret;
    }

    function getCalendari() {
        return $this->calendari;
    }

    private function setCampi() {
        $db_query = "SELECT * FROM " . tabella_campi;
        $result = wrap_db_query($db_query);
        $this->campi = mysqli_fetch_all($result);
    }

    function getCampi() {
        return $this->campi;
    }

    private function setDayCalendar() {
        $query = "SELECT valore FROM impostazioni WHERE chiave = 'giorninelcalendario'";
        $res = wrap_db_query($query);
        $count = $this->getSquaresInt();
        if ($count >= 3) {
            $check = round(mysqli_fetch_row($res)[0] / $count);
            if ($check == 0) {
                $this->dayCalendar = 1;
            } else {
                if ($count == 3) {
                    $this->dayCalendar = $check + 1;
                } else {

                    $this->dayCalendar = $check;
                }
            }
        } else {
            $this->dayCalendar = mysqli_fetch_row($res)[0];
        }
    }

    function getDayCalendar() {
        return $this->dayCalendar;
    }

    private function getMaxMinOra() {
        $count = 0;
        $db_query = "SELECT CONCAT('SELECT MAX(ora) AS max, MIN(ora) AS min FROM ', table_name, ' WHERE `0` IS NOT NULL OR `1` IS NOT NULL"
                . " OR `2` IS NOT NULL OR `3` != NULL OR `4` IS NOT NULL OR `5` IS NOT NULL OR `6` IS NOT NULL ')"
                . " AS statement FROM information_schema.tables WHERE table_name LIKE 'cal_camp%' GROUP BY table_name";
        $result = wrap_db_query($db_query);
        $p = mysqli_fetch_all($result);
        $query = "SELECT MIN(min) as mins, MAX(max) as maxs FROM ( ";
        foreach ($p as $x) {
            //return $x[0];
            $query .= $x[0];
            if ($count != count($p) - 1) {
                $query .= "\n UNION \n";
            } else {
                $query .= " ) AS temp";
            }
            $count++;
        }
        $res = wrap_db_query($query);
        $dud = mysqli_fetch_assoc($res);
        $this->max = $dud["maxs"];
        $this->min = $dud["mins"];
    }

    private function setPeriodi() {
        $db_query = "SELECT * FROM tariffe_date";
        $res = wrap_db_query($db_query);
        $this->periodi = mysqli_fetch_all($res);
    }

    /*
     * FINE GET E SET
     */

    function isAvaiable() {
        foreach ($this->campi as $x) {
            if ($x[2] == "1") {
                return true;
            }
        }
        return false;
    }

    /*
     * STRUTTURA TABELLONE
     */

    /**
     * ShowCalendario
     */
    function showCalendario() {
        $content = "<table class='striped'>";
        $content .= $this->getTableHead();
        $content .= $this->getBody();
        $content .= "</table>";
        return $content;
    }

    /**
     * Headtabella
     * @return string
     */
    private function getTableHead() {
        $content = "<thead class='center'>";
        $content .= $this->getDaysOnCalendar();
        $content .= $this->getSquares();
        $content .= "</thead>";
        return $content;
    }

    /**
     * Costruisce il numero di giorni
     * @return string
     */
    function getDaysOnCalendar() {
        if (isset($_REQUEST['date'])) {
            $date = date_create($_REQUEST['date']);
        } else {
            $date = new DateTime();
        }
        $content = "<tr><th class='orario'> </th>";
        $colspan = $this->getSquaresInt();
        $giorni = $this->dayCalendar;
        $date->modify('-1 day');
        for ($i = 0; $i < $giorni; $i++) {
            $date->modify('+1 day'); 
            $content .= "<th colspan='$colspan' class='center'>"
                    . strftime('%a %d %b', $date->getTimestamp())
                    . "</th>";
        }
        $content .= "</tr>";
        return $content;
    }

    /**
     * Contatore numeri campi
     * @return int
     */
    private function getSquaresInt() {
        $int = 0;
        foreach ($this->campi as $x) {
            if ($x[2] != 0) {
                //if ($this->canWalk($x[0])) {
                $int++;
                //}
            }
        }
        return $int;
    }

    /**
     * Seconda riga della tabella
     * @return string
     */
    function getSquares() {
        $content = "<tr><th class='orario center'>Orario</th>";
        for ($i = 0; $i < $this->dayCalendar; $i++) {
            foreach ($this->getCampi() as $x) {
                if ($x[2] == 1) {
                    //if ($this->canWalk($x[0])) {
                    $content .= "<th class='center'>" . $x[1] . "</th>";
                    //}
                }
            }
        }
        $content .= "</tr>";
        return $content;
    }

    /**
     * Corpo del tabellone
     * @return string
     */
    function getBody() {
        $content = "<tbody>";
        $tNow = strtotime($this->min);
        $tEnd = strtotime($this->max);
        while ($tNow <= $tEnd) {
            $content .= '<tr>'
                    . '<td class="orario center">';
            $content .= date("H:i", $tNow) . " - ";
            $tMid = date("H/i", $tNow);
            $tNow = strtotime('+30 minutes', $tNow);
            $content .= date("H:i", $tNow);
            $content .= '</td>';
            $content = $this->riempiBottoni($content, $tMid);
            $content .= '</tr>';
        }
        $content .= "</tbody>";
        return $content;
    }

    /**
     * Riempie ogni spazio della tabella
     * @param string $content
     * @param int $day
     * @param int $month
     * @param int $year
     * @return string
     */
    function riempiBottoni($content, $tMid) {
        $conta = $this->getSquaresInt();
        if (isset($_REQUEST['date'])) {
            $date = date_create($_REQUEST['date']);
        } else {
            $date = new DateTime();
        }
        for ($i = 0; $i < $this->getDayCalendar(); $i++) {
            foreach ($this->campi as $x) {
                if ($x[2] != 0) {
                    //if ($this->canWalk($x[0])) {
                    $content .= "<td>" . $this->isWalkable($date->format('Y'), $date->format('m'), $date->format('d'), $tMid, $x[0]) . "</td>";
                    //}
                }
            }
            $date->modify('+1 day');
        }
        return $content;
    }

    /**
     * Check tempo sul bottone
     * @param type $year
     * @param type $week
     * @param type $day
     * @param type $time
     * @param type $j
     * @return string
     */
    function isWalkable($year, $week, $day, $time, $campo) {
        $tim = explode("/", $time);
        $uDCheck = strtotime($week . "/" . $day . "/" . $year);
        $uHCheck = strtotime($tim[0] . ":" . $tim[1] . ":00");
        $tOra = date_create();
        $uDOra = strtotime(date_format($tOra, 'm/d/Y'));
        $uHOra = strtotime(date_format($tOra, 'H:i:s'));
        $periodo = $this->getPeriodoCampo($campo, "$week/$day");
        if (!$periodo) {
            return "<a class='btn disabled center'>Chiuso</a>";
        } else {
            if ($uDCheck < $uDOra) {
                return "<a class='btn disabled center'>Passato</a>";
            } else {
                if ($uDCheck == $uDOra) {
                    if ($uHCheck < $uHOra) {
                        return "<a class='btn disabled center'>Passato</a>";
                    } else {
                        $tariffa = $this->loadCalendarioCampo($campo, $uDCheck, $uHCheck, $periodo);
                        if ($tariffa != NULL) {
                            if ($this->canSub($tariffa)) {
                                return $this->checkReservation($year, $week, $day, $time, $campo, $tariffa);
                            } else {
                                return "<a class='btn center blue'>No</a>";
                            }
                        } else {
                            return "<a class='btn disabled center'>Chiuso</a>";
                        }
                    }
                } else {
                    $tariffa = $this->loadCalendarioCampo($campo, $uDCheck, $uHCheck, $periodo);
                    if ($tariffa != NULL) {
                        if ($this->canSub($tariffa)) {
                            return $this->checkReservation($year, $week, $day, $time, $campo, $tariffa);
                        } else {
                            return "<a class='btn center red'>No</a>";
                        }
                    } else {
                        return "<a class='btn disabled center'>Chiuso</a>";
                    }
                }
            }
        }
    }

    private function canSub($tariffa) {
        $check = false;
        $arr = $this->getTariffa($tariffa);
        $privi = $this->priv;
        if ($privi != "guest") {
            foreach ($arr as $x) {
                if ($x["$privi"] != NULL) {
                    $check = true;
                }
            }
        }
        return $check;
    }

    /**
     * Check delle prenotazioni in quel campo/data
     * @param type $year
     * @param type $week
     * @param type $day
     * @param type $time
     * @param type $j
     * @return string
     */
    function checkReservation($year, $week, $day, $time, $campo, $tariffa) {
        $query = "SELECT * FROM prenotazioni WHERE reservation_year = '$year' AND reservation_week = '$week' AND reservation_day = '$day' AND reservation_time = '$time' AND reservation_campo = '$campo' ";
        $res = wrap_db_query($query);
        $result = mysqli_fetch_all($res);
        if ($result != null) {
            $count = $this->attivita->getMaxUserById($result[0][12]);
            $int = $count - count($result) - $result[0][13];
            if ($int <= 0) {
                return "<a class='btn occupato red' value='$year/$week/$day/$time/$campo'> Occupato </a>";
            } else {
                // TODO: check se può iscriversi / prenotare
                return "<a class='btn waves-effect " . $this->getColoreTariffa($tariffa) . " waves-light btnprenota center' href='#prenotamodal' value='$year/$week/$day/$time/$campo/$tariffa/" . $result[0][12] . "'> Disp. " . $int . " posti</a>";
            }
        } else {
            return "<a class='btn waves-effect " . $this->getColoreTariffa($tariffa) . " waves-light btnprenota center' href='#prenotamodal' value='$year/$week/$day/$time/$campo/$tariffa'> Prenota </a>";
        }
    }

    /**
     * Controlla se quel slot è attivo oppure no
     * @param type $campo
     * @param type $data
     * @param type $ora
     * @param type $periodo
     * @return boolean
     */
    function loadCalendarioCampo($campo, $data, $ora, $periodo) {
        $weekday = date("w", $data);
        $arr = array();
        foreach ($this->setCalendario($campo, $periodo) as $x) {
            $checkTime = strtotime($x['ora']);
            if ($ora == $checkTime) {
                if ($x["$weekday"]) {
                    return $x["$weekday"];
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * Controllo data per il periodo
     * @param type $id
     * @param type $data
     * @return boolean
     */
    function getPeriodoCampo($id, $data) {
        $checkDate = new DateTime($data);
        foreach ($this->periodi as $x) {
            if ($x[1] == $id) {
                $lowerBound = new DateTime($x[3]);
                $upperBound = new DateTime($x[4]);
                if ($lowerBound < $upperBound) {
                    if ($lowerBound <= $checkDate && $checkDate <= $upperBound) {
                        return $x[2];
                    }
                } else {
                    if ($checkDate <= $upperBound || $checkDate >= $lowerBound) {
                        return $x[2];
                    }
                }
            }
        }
        return false;
    }

}
