<?php

class TabSetting {

    private $prenotazioni;
    private $nomicampi;
    private $prodotti;
    private $conn;

    /**
     * Costruttore
     */
    public function __construct($conn) {
        include "../includes/database.php";
        setlocale(LC_TIME, 'ita', 'it_IT');
        $this->conn = $conn;
        $this->setPrenotazioni();
        $this->setNomiCampi();
        $this->setProdotti();
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
    }

    function setProdotti() {
        $query = "SELECT oid, onome FROM opzioni";
        $res = wrap_db_query($query);
        $this->prodotti = mysqli_fetch_all($res);
    }

    function setPrenotazioni() {
        $query = "SELECT * FROM prenotazioni";
        $res = wrap_db_query($query);
        $this->prenotazioni = mysqli_fetch_all($res);
    }

    function getNiceDate($anno, $mese, $giorno) {
        $date = new DateTime($anno . "-" . $mese . "-" . $giorno);
        return $date->format('d/m/y');
    }

    function getNiceHour($time) {
        $arr = explode("/", $time);
        $h = $arr[0];
        $m = $arr[1];
        $date = new DateTime($h . ":" . $m);
        $content = $date->format('H:i');
        $content .= " alle ";
        $date->modify('+30 minute');
        $content .= $date->format('H:i');
        return $content;
    }

    function costruisciTabella() {
        $content = '<table class="striped" id="prenotazionitab">'
                . '<thead>'
                . '<th> Chi </th>'
                . '<th> Quando </th>'
                . '<th> Giorno </th>'
                . '<th> Orario </th>'
                . '<th> Campo </th>'
                . '<th> Opzioni </th>'
                . '<th> Pagato </th>'
                . '</thead>'
                . '<tbody>';
        foreach ($this->prenotazioni as $x) {

            $content .= $this->getRow($x);
        }
        $content .= "</tbody> </table>";
        return $content;
    }

    function getRow($x) {
        return "<tr> <td> $x[9] </td> <td> $x[1] </td> <td> " . $this->getNiceDate($x[2], $x[3], $x[4]) . " </td> <td> " . $this->getNiceHour($x[5]) . " </td> <td> " . $this->getNomeCampo($x[6]) . " </td> <td> " . $this->getProdotto($x[10]) . " </td> <td> " . $this->getPagato($x[7]) . " </td> </tr>";
    }

    function getProdotto($id) {
        $content = "";
        $arr = explode(",", $id);
        $int = 0;
        if ($id == null) {
            return "Nessuno";
        }
        foreach ($arr as $x) {
            foreach ($this->prodotti as $k) {
                if ($x == $k[0]) {
                    if ($int > 0) {
                        $content .= ", $k[1]";
                    } else {
                        $content .= $k[1];
                        $int++;
                    }
                }
            }
        }
        return $content;
    }

    function getPagato($a) {
        if ($a == 0) {
            return "No";
        } else {
            return "Si";
        }
    }

    function getPrenotazioni() {
        return $this->prenotazioni;
    }

    function getNomeCampo($id) {
        foreach ($this->nomicampi as $x) {
            if ($x[0] == $id) {
                return $x[1];
            }
        }
    }

    function setNomiCampi() {
        $query = "SELECT cid, cnome FROM campo";
        $res = wrap_db_query($query);
        $this->nomicampi = mysqli_fetch_all($res);
    }

}
