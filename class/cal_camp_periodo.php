<?php
//SET @inizio = TIME_FORMAT("08:00", '%H:%i');
//SET @fine = TIME_FORMAT("12:30", '%H:%i');
//SET @campoId = "1";
//SET @periodoId = "1";
//SET @giorno = "2";
//SET @tariffaId = "1";
//  	SET @c = CONCAT('UPDATE `cal_camp', @campoId,'_pe', @periodoId,'` SET `', @giorno,'`=', @tariffaId,' WHERE `ora`="', @inizio,'"');
//   	/*PREPARE stmt FROM @c;
//   	EXECUTE stmt;
//   	DEALLOCATE PREPARE stmt;*/
//    SELECT @c;
//   	SET @currentdate = ADDTIME(@inizio, "00:30:00");

class AggiornaCal{
    public function __construct() {
        include "../includes/databas.php";
        include "../includes/declares.php";
        setlocale(LC_TIME, 'ita', 'it_IT');
        $conn = wrap_db_connect();
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
    }
    
    /**
     * Aggiorna il calendario settimanale
     * @param type $inizio
     * @param type $fine
     * @param type $campoId
     * @param type $periodoId
     * @param type $giorno
     * @param type $tariffaId
     * @return string
     */
    public function aggiornaCalendario($inizio, $fine, $campoId, $periodoId, $giorno, $tariffaId){
        $arr = array();
        $oraInizio = strtotime($inizio);
        $oraFine = strtotime($fine);
        $db_query = "UPDATE cal_camp".$campoId."_pe".$periodoId." SET `".$giorno."` = '".$tariffaId."' WHERE `ora` in (";
        while($oraInizio < $oraFine){
            $db_query .= "'".date("H:i", $oraInizio)."' ";
            $oraInizio = strtotime('+30 minutes', $oraInizio);
            if($oraInizio >= $oraFine){
                $db_query .= ");";
            } else {
                $db_query .= " , ";
            }
        }
        $result = wrap_db_query($db_query);
        return $db_query;
    }
}
