<?php
include 'includes/db2.php';
$conn = wrap_db_connect();
$query = "CREATE TABLE IF NOT EXISTS`anagrafica` (
  `uid` int(11) NOT NULL,
  `unome` varchar(50) NOT NULL,
  `ucognome` varchar(50) NOT NULL,
  `udata` date NOT NULL,
  `udatareg` date NOT NULL,
  `utelefono` int(11) NOT NULL,
  `uemail` varchar(50) NOT NULL,
  `tid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

wrap_db_query($query);

$db_query = "INSERT INTO `anagrafica` (`uid`, `unome`, `ucognome`, `udata`, `udatareg`, `utelefono`, `uemail`, `tid`) VALUES
(0, 'Riccardo', 'Strazzer', '1996-01-08', '2017-06-05', 123, 'strez08@gmail.com', 1);";

wrap_db_query($db_query);

$db_query = "CREATE TABLE `campo` (
  `cid` int(11) NOT NULL,
  `cnome` varchar(50) NOT NULL,
  `cprezzo` int(11) NOT NULL,
  `stato` tinyint(4) NOT NULL,
  `orainizio` time NOT NULL,
  `orafine` time NOT NULL,
  `oid` int(11) NOT NULL,
  `tid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

wrap_db_query($db_query);

$db_query = "INSERT INTO `campo` (`cid`, `cnome`, `cprezzo`, `stato`, `orainizio`, `orafine`, `oid`, `tid`) VALUES
(0, 'Terra Due', 500, 1, '09:00:00', '23:00:00', 1, 2),
(1, 'Terra Normale', 1000, 1, '08:00:00', '20:00:00', 1, 1);";

wrap_db_query($db_query);

$db_query = "CREATE TABLE `impostazioni` (
  `iid` int(11) NOT NULL,
  `chiave` varchar(64) NOT NULL,
  `valore` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

wrap_db_query($db_query);

$db_query = "INSERT INTO `impostazioni` (`iid`, `chiave`, `valore`) VALUES
(1, 'nomeamministratore', 'Riccardo Strazzer'),
(2, 'giorninelcalendario', '5');";


wrap_db_query($db_query);

$db_query = "
CREATE TABLE `opzioni` (
  `oid` int(11) NOT NULL,
  `onome` varchar(50) NOT NULL,
  `oprezzo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
wrap_db_query($db_query);

$db_query = "CREATE TABLE `prenotazioni` (
  `reservation_id` int(10) NOT NULL,
  `reservation_made_time` datetime NOT NULL,
  `reservation_year` smallint(4) NOT NULL,
  `reservation_week` tinyint(2) NOT NULL,
  `reservation_day` tinyint(1) NOT NULL,
  `reservation_time` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `reservation_campo` int(11) NOT NULL,
  `reservation_user_id` int(10) NOT NULL,
  `reservation_user_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

wrap_db_query($db_query);

$db_query = "INSERT INTO `prenotazioni` (`reservation_id`, `reservation_made_time`, `reservation_year`, `reservation_week`, `reservation_day`, `reservation_time`, `reservation_campo`, `reservation_user_id`, `reservation_user_name`) VALUES
(15, '2017-06-06 19:07:24', 2017, 6, 8, '09/30', 0, 0, 'Riccardo'),
(14, '2017-06-06 19:07:20', 2017, 6, 8, '09/00', 0, 0, 'Riccardo'),
(13, '2017-06-06 18:35:07', 2017, 6, 7, '08/30', 1, 0, 'Riccardo');";
wrap_db_query($db_query);


wrap_db_query("
CREATE TABLE `tipologia` (
  `tid` int(11) NOT NULL,
  `tnome` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
wrap_db_query("INSERT INTO `tipologia` (`tid`, `tnome`) VALUES
(1, 'Socio'),
(2, 'Amministratore');");
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);
wrap_db_query($db_query);

