<?php
session_start();
?><!DOCTYPE html>
<html>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/css/materialize.min.css">
        <link type="text/css" rel="stylesheet" href="ajax/access/login.css"  media="screen,projection"/>
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
        <!--<link rel="stylesheet" type="text/css" href="css/timepicki.css">-->
        <link type="text/css" rel="stylesheet" href="css/jquery.auto-complete.css" media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="css/cal.css" media="screen,projection"/>



        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    </head>
    <body class="unselectable">
        <div class="conte">

        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="js/it_IT.js"></script>
        <script tyep="text/javascript" src="js/main.js"></script>
        <!--<script tyep="text/javascript" src="js/picker.date.js"></script>-->
        <script type="text/javascript" src="js/jquery.auto-complete.min.js"></script>
        <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
    </body>
</html>       
