/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Monda
 * Created: 31-mag-2017
 */

CREATE TABLE cal (
        id                      INTEGER PRIMARY KEY,
        year                    INTEGER NOT NULL,
        month                   INTEGER NOT NULL,
        day                     INTEGER NOT NULL,
        half                    INTEGER NOT NULL,
        hour                    INTEGER NOT NULL,
        checked			TINYINT(1) DEFAULT 0 -- Check if is reserved
) Engine=MyISAM;

DROP PROCEDURE IF EXISTS fill_date_dimension;
DELIMITER //
CREATE PROCEDURE fill_date_dimension(IN giorno DATETIME)
BEGIN
    DECLARE currentdate DATETIME;
    DECLARE stoppete DATETIME;
    SET currentdate = DATE_FORMAT(giorno, '%Y-%m-%d 08:00:00');
    SET stoppete = DATE_FORMAT(giorno, '%Y-%m-%d 20:00:00');
    WHILE currentdate < stoppete DO
        INSERT INTO cal (id,year,month,day,half,hour,checked) VALUES (
            concat(DATE_FORMAT(currentdate, '%Y'),DATE_FORMAT(currentdate, '%m'),DATE_FORMAT(currentdate, '%d'),DATE_FORMAT(currentdate, '%H'),DATE_FORMAT(currentdate, '%i')),
            DATE_FORMAT(giorno, '%Y'),
            DATE_FORMAT(giorno, '%m'),
            DATE_FORMAT(giorno, '%d'),
            DATE_FORMAT(currentdate, '%i'),
            DATE_FORMAT(currentdate, '%H'),
            0);
        SET currentdate = DATE_ADD(currentdate, INTERVAL 30 MINUTE);
    END WHILE;
END
//
DELIMITER ;

-- c'è da sistemare la data e gli orari da andare a prendere dentro alle tabelle
TRUNCATE TABLE cal;

CALL fill_date_dimension(curdate(),(curdate() + INTERVAL 21 DAY));
OPTIMIZE TABLE cal;

TRUNCATE TABLE cal;
CALL fill_date_dimension('2017-05-30');
OPTIMIZE TABLE cal;

BEGIN
    SET @giorno = DATE_FORMAT(curdate(), '%d');
    SET @mese = DATE_FORMAT(curdate(), '%m');
    SET @anno = DATE_FORMAT(curdate(), '%Y');
    DELETE FROM cal WHERE day = @giorno AND month = @mese AND year = @anno;
    SET @data = DATE_ADD(curdate(), INTERVAL 14 DAY);
    CALL fill_date_dimension(@data);
END

TRUNCATE TABLE cal;
CALL fill_date_dimension('2017-06-05');
CALL fill_date_dimension('2017-06-06');
CALL fill_date_dimension('2017-06-07');
CALL fill_date_dimension('2017-06-08');
CALL fill_date_dimension('2017-06-09');
CALL fill_date_dimension('2017-06-10');
CALL fill_date_dimension('2017-06-11');
CALL fill_date_dimension('2017-06-12');
CALL fill_date_dimension('2017-06-13');
CALL fill_date_dimension('2017-06-14');
CALL fill_date_dimension('2017-06-15');
CALL fill_date_dimension('2017-06-16');
CALL fill_date_dimension('2017-06-17');
CALL fill_date_dimension('2017-06-18');
OPTIMIZE TABLE cal;

CREATE EVENT new_day
	ON SCHEDULE
    EVERY 48 DAY_MINUTE
    DO
    	CALL newgiorno();






DROP PROCEDURE IF EXISTS newcampoorario;
DELIMITER //
CREATE PROCEDURE newcampoorario(IN campoNome TEXT)
BEGIN
    SET @orainizio = DATE_FORMAT(giorno, '%Y-%m-%d');
    SET @orafine = DATE_FORMAT(giorno, '%Y-%m-%d');
    SET @stringa = CONCAT('CREATE TABLE ',@campoNome);
    PREPARE stmt FROM @stringa;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END
//
DELIMITER ;