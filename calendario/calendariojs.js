$(document).ready(function () {
    getPage();
    loadNav();
    loadCalendario();
    /*$.ajax({
     url: "calendarioajax.php",
     type: "POST",
     data: {
     data: $
     }
     })*/
});

function getPage() {
    $.ajax({
        url: "../ajax/getpage.html",
        type: "POST",
        success: function (result) {
            $(".conte").html(result);
            loadNav();
            $('.modal').modal();
        }
    });
}

function loadNav() {
    $.ajax({
        url: "getnav.php",
        type: "POST",
        success: function (result) {
            $('#navi').html(result);
            window.location.hash = "#home";
            $("#sidenav").sideNav({
                closeOnClick: true
            });
            $(".dropdown-button").dropdown();
            $(".dropdown-button").html($('.selezionato').html() + '<i class="material-icons right">arrow_drop_down</i>');
        }
    });
}

$(document).on('click', "#campidropdown a", function () {
    $(".selezionato").attr("class", "");
    $(this).attr("class", "selezionato");
    $(".dropdown-button").html($('.selezionato').html() + '<i class="material-icons right">arrow_drop_down</i>');
    loadCalendario();
});

function loadCalendario() {
    window.location.hash = "#home";
    var ismobile = false;
    var sData = getDataFromValue();
    /*if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
     ismobile = true;
     }*/
    $('.btnavanti').attr('value', aggiornaData(sData, 1));
    $('.btnindietro').attr('value', aggiornaData(sData, -1));
    $.ajax({
        url: "calendarioajax.php",
        type: "POST",
        data: {
            date: sData,
            mobile: ismobile,
            campo: $(".selezionato").attr('value')
        },
        success: function (result) {
            $("#divtabella").html(result);
            $('.btnindietro').show();
            $('.btnavanti').show();
            $('#datacalendario').show();
            $('#nomecampo').html($('#dropdowncampi').has('sel', 'selezionato').html());
            $('.dropdown-button').show();
        }
    });

}

function createDate(sData) {
    var res = sData.split("-");
    return res[2] + "/" + res[1] + "/" + res[0];
}

function getDataFromValue() {
    var sValue = $("#datacalendario").text();
    var res = sValue.split("/");
    return res[2] + "-" + res[1] + "-" + res[0];
}

function aggiornaData(sDate, iAddDays) {
    var d = new Date(sDate);
    d.setDate(d.getDate() + parseInt(iAddDays));
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = d.getFullYear() + '-' +
            (month < 10 ? '0' : '') + month + '-' +
            (day < 10 ? '0' : '') + day;
    return output;
}