<?php

class Amministratore {

    /**
     * Costruttore
     */
    public function __construct() {
        include "../includes/database.php";
        setlocale(LC_TIME, 'ita', 'it_IT');
        $conn = wrap_db_connect();
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
    }

    function getPrivileges() {
        $query = "SELECT tid FROM anagrafica, users WHERE username = '" . $_SESSION['user'] . "' AND anagrafica.uid = users.uid";
        $res = wrap_db_query($query);
        $result = wrap_db_fetch_array($res);
        if ($result['tid'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    function getNextColor() {
        $colore = $_REQUEST['colore'];
        $id = $_REQUEST['tariffa'];
        $colori = array('red', 'red accent-1', 'pink', 'pink darken-4', 'deep-purple lighten-3', 'deep-purple', 'indigo', 'blue',
            'light-blue', 'cyan', 'teal', 'green', 'light-green', 'lime', 'lime darken-4', 'yellow darken-3',
            'amber', 'orange', 'orange darken-4', 'deep-orange', 'brown', 'grey', 'grey darken-3', 'blue-grey', 'blue-grey darken-2');

        $get = array_search($colore, $colori);

        if ($get < (count($colori)) - 1) {
            $get++;
        } else {
            $get = 0;
        }

        $colore = $colori[$get];
        $this->saveColor($colore, $id);
        
        return $colore;
    }
    
    private function saveColor($colore, $id) {
        $db_query = "UPDATE tariffe SET colore = '$colore' WHERE id = '$id'";
        $res = wrap_db_query($db_query);
    }

    function getCampi() {
        $db_query = "SELECT * FROM campo";
        $res = wrap_db_query($db_query);
        $result = mysqli_fetch_all($res, MYSQLI_ASSOC);
        $content = "";
        $bool = " sel='selezionato' ";
        foreach ($result as $x) {
            if ($x['stato'] == 1) {
                $content .= "<a" . $bool . " value='" . $x['cid'] . "' class='w3-bar-item w3-button'>" . $x['cnome'] . "</a>";
                $bool = "";
            }
        }
        $content .= "";
        return $content;
    }

}
