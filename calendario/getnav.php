<?php
session_start();
if (isset($_SESSION['user'])) {

    include 'pannelloadmin.php';
    $admin = new Amministratore();
    ?>
    <div class='w3-top'>
        <div class="w3-bar">
            <a class="w3-bar-item w3-circle btnindietro"><i class="material-icons">fast_rewind</i></a>
            <a class="w3-bar-item" id='datacalendario'><?php echo date('d/m/Y') ?></a>
            <a id='title'></a>
            <a class="w3-bar-item w3-circle btnavanti"><i class="material-icons ">fast_forward</i></a>


            <a class="w3-bar-item w3-button w3-right" id="usersession">Logout</a>
            <?php
            if ($admin->getPrivileges()) {
                ?>
                <a class="w3-bar-item w3-button w3-right" id="campi">Campi</a>
                <a class="w3-bar-item w3-button w3-right" id="tariffe">Tariffe</a>
                <a class="w3-bar-item w3-button w3-right" id="extra">Prodotti</a>
                <a class="w3-bar-item w3-button w3-right" id="utenti">Utenti</a>
                <?php
            }
            ?>
            <a class="w3-bar-item w3-button w3-right" id="calendario">Calendario</a></li>
            <div class="w3-dropdown-hover w3-right">
                <button class="w3-button" id="nomecampo">Campi</button>
                <div class="w3-dropdown-content w3-bar-block w3-card-4" id="dropdowncampi">
                    <?php
                    echo $admin->getCampi();
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
} else {
    die("BadRequest");
}
?>



