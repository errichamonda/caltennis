<?php

//AJAX

session_start();
if (isset($_REQUEST['val']) && !empty($_SESSION['user'])) {
    include '../class/prenota.php';
    $modal = new Prenota();
    $value = $_REQUEST['val'];

    $arr = explode("/", $value);
    $year = $arr[0];
    $week = $arr[1];
    $day = $arr[2];
    $h = $arr[3];
    $m = $arr[4];
    $time = $arr[3] . "/" . $arr[4];
    $campo = $arr[5];
    $tariffa = $arr[6];
    //$modal->setCampo($campo);
    //$modal->setTariffa("$mese/$giorno");
    $aaa = $modal->isWalkable($year, $week, $day, $time, $campo);
    if ($aaa) {
        if ($modal->checkReservation($year, $week, $day, $time, $campo)) {
            die("<div class='modal-content'> <h4> Spazio occupato </h4> </div>");
        } else {
            if ($modal->isEvento($year, $week, $day, $time, $campo)) {
                $content = "<div class='modal-content'>"
                        . "<h5 class='center'>" . $modal->getCampoNome($campo) . "</h5>"
                        . "<p class='center'>" . $modal->getNomeAttivita() . "</p>"
                        . $modal->getNiceDate($year, $week, $day, $h, $m)
                        . "<p class='flow-text center green-text' id='loadph'> Puoi prenotare un posto su questa attività </p>"
                        . "<p class='center' id='prezzomodal'>" . $modal->getPrezzo() . "</p></div>"
                        . $modal->getFooterIscrizioneEvento($value)
                        . "</div>";
                echo $content;
            } else {
                $content = "<div class='modal-content'>"
                        . "<h5 class='center'>" . $modal->getCampoNome($campo) . "</h5>"
                        . $modal->getNiceDate($year, $week, $day, $h, $m)
                        . "<p class='flow-text center green-text' id='loadph'> Questo campo è ancora libero </p>"
                        . "<div class='row'>";
                $content .= $modal->getBody();
                $content .= "</div>"
                        . $modal->getFooter($value)
                        . "</div>";
                echo $content;
            }
        }
    } else {
        die("Non prenotabile");
    }
} else {
    die("Bad request");
}

