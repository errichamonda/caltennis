<?php

session_start();
if (isset($_REQUEST['val']) /* && !empty($_SESSION['user']) */) {
    include '../class/prenota.php';
    $modal = new Prenota();
    $value = $_REQUEST['val'];

    $arr = explode("/", $value);
    $year = $arr[0];
    $week = $arr[1];
    $day = $arr[2];
    $h = $arr[3];
    $m = $arr[4];
    $time = $arr[3] . "/" . $arr[4];
    $campo = $arr[5];
    $tariffa = $arr[6];
    $attivita = $arr[7];
    $extra = $arr[8];



    if ($_REQUEST['what'] == "fill") {
        $aaa = $modal->isWalkable($year, $week, $day, $time, $campo);
        if ($aaa) {
            if ($modal->checkReservation($year, $week, $day, $time, $campo)) {
                die("<div class='modal-content'> <h4> Spazio occupato </h4> </div>");
            } else {
                if ($modal->getMaxUser($attivita) != "1") {
                    $content = "<div class='modal-content'>"
                            . "<h5 class='center'>" . $modal->getCampoNome($campo) . "</h5>"
                            . $modal->getNiceDate($year, $week, $day, $h, $m)
                            . "<div class='row'>"
                            . "<div class='input-field'>"
                            . "<input type='hidden' id='user_id' value=''>"
                            . "<i class='material-icons prefix'>person</i>"
                            . "<input id='user_autocomplete' type='text'>"
                            . "<label for='user_autocomplete'>Utente</label>"
                            . "</div>"
                            . "<div class='row'>"
                            . "<a class='btn btn-flat waves-effect addguest col s3'>Guest</a>"
                            . "<div id='utentiplaceholder' class='center col s9'>"
                            . $modal->chipCurrentUser()
                            . "</div>"
                            . "</div>"
                            . "</div>"
                            . $modal->getFooterFill($value)
                            . "</div>";
                    echo $content;
                } else {
                    $content = "<div class='modal-content'>"
                            . "<h5 class='center'>" . $modal->getCampoNome($campo) . "</h5>"
                            . $modal->getNiceDate($year, $week, $day, $h, $m)
                            . "<h5 class='center'>" . $modal->getRiassunto($attivita, $extra, $campo) . "</h5>"
                            . "<div id='utentiplaceholder' class='center'>"
                            . $modal->chipCurrentUser()
                            . "</div>"
                            . $modal->getFooterEvento($value)
                            . "</div>";
                    echo $content;
                }
            }
        } else {
            die("Spazio Occupato");
        }
    } elseif ($_REQUEST['what'] == "riassunto") {
        $aaa = $modal->isWalkable($year, $week, $day, $time, $campo);
        if ($aaa) {
            if ($modal->checkReservation($year, $week, $day, $time, $campo)) {
                die("<div class='modal-content'> <h4> Spazio occupato </h4> </div>");
            } else {

                $content = "<div class='modal-content'>"
                        . "<h5 class='center'>" . $modal->getCampoNome($campo) . "</h5>"
                        . $modal->getNiceDate($year, $week, $day, $h, $m)
                        . "<h5 class='center'>" . $modal->getRiassunto($attivita, $extra, $_REQUEST['utenti']) . "</h5>"
                        . "<div class='chipplaceholder center'>" . $modal->getChipRiassunto($_REQUEST['utenti']) . "</div>"
                        . $modal->getFooterEvento($value)
                        . "</div>";
                echo $content;
            }
        }
    } else {
        die("BadRequest");
    }
}