<?php

//lookup delle prenotazioni e eventuale cancellazione

session_start();
if (isset($_SESSION['user'])) {
    include '../class/prenota.php';
    $modal = new Prenota();
    if ($modal->getPrivileges() == "0" && isset($_REQUEST['val'])) {
        $arr = $_REQUEST['val'];
        $arr = explode("/", $arr);
        $year = $arr[0];
        $week = $arr[1];
        $day = $arr[2];
        $h = $arr[3];
        $m = $arr[4];
        $time = $arr[3] . "/" . $arr[4];
        $campo = $arr[5];
        $content = "";
        if ($_REQUEST['what'] == "lookup") {
            $content .= "<div class='modal-content'>"
                    . "<h5 class='center'>" . $modal->getCampoNome($campo) . "</h5>"
                    . $modal->getNiceDate($year, $week, $day, $h, $m)
                    . "<div class='row'>"
                    . "<div class='col s2'> </div>"
                    . $modal->getLookup($year, $week, $day, $h, $m, $campo)
                    . "</div>"
                    . "<div class='modal-footer'><a class='modal-action modal-close waves-effect waves-green btn-flat'>Chiudi</a></div>";
        } elseif ($_REQUEST['what'] == 'delete') {
            $content .= "<div class='modal-content'>"
                    . "<h3 class='center red'>Sicuro di voler eliminare questa prenotazione?</h3>"
                    . "<div class='row'>"
                    . "<div class='col s4'>"
                    . "<h5 class='center'>" . $modal->getCampoNome($campo) . "</h5>"
                    . $modal->getNiceDate($year, $week, $day, $h, $m)
                    . "</div>"
                    . $modal->getLookup($year, $week, $day, $h, $m, $campo)
                    . "</div>"
                    . "<div class='modal-footer'><a id='eliminapreno' class='modal-action modal-close waves-effect waves-red btn-flat' value='".$_REQUEST['val']."'>Elimina</a></div>";
        } elseif ($_REQUEST['what'] == "elimina") {
            $modal->deletePrenotazione($year, $week, $day, $h, $m, $campo);
        }
        echo $content;
    } else {
        die("Non Premesso");
    }
} else {
    die("BadRequest");
}

