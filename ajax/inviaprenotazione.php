<?php

session_start();
if (!empty($_SESSION['user'])) {
    if (isset($_REQUEST['val'])) {

        include '../class/prenota.php';
        $modal = new Prenota();
        $value = $_REQUEST['val'];
        if ($_REQUEST['what'] == "noev") {
            $arr = explode("/", $value);
            $year = $arr[0];
            $week = $arr[1];
            $day = $arr[2];
            $h = $arr[3];
            $m = $arr[4];
            $time = $arr[3] . "/" . $arr[4];
            $campo = $arr[5];
            $tariffa = $arr[6];
            $tipo = $arr[7];
            if ($arr[8] == " ") {
                $prodotti = "";
            } else {
                $prodotti = $arr[8];
            }
            $aaa = $modal->isWalkable($year, $week, $day, $time, $campo);
            if ($aaa) {
                if ($modal->checkReservation($year, $week, $day, $time, $campo)) {
                    die("<div class='modal-content'> <h4> Spazio occupato </h4> </div>");
                } else {
                    $content = "<div class='modal-content'>"
                            . "<h5 class='center'>" . $modal->getCampoNome($campo) . "</h5>"
                            . $modal->getNiceDate($year, $week, $day, $h, $m)
                            . "<h5 class='center'>" . $modal->setPrenota($year, $week, $day, $time, $campo, $prodotti, $tipo, $tariffa, $_REQUEST['utenti']) . "</h5>"
                            . $modal->getFooterEvento($value)
                            . "</div>";
                    echo $content;
                }
            } else {
                die("Spazio Occupato");
            }
        }
        if ($_REQUEST['what'] == "ev") {
            $arr = explode("/", $value);
            $anno = $arr[0];
            $mese = $arr[1];
            $giorno = $arr[2];
            $h = $arr[3];
            $m = $arr[4];
            $ora = $arr[3] . "/" . $arr[4];
            $campo = $arr[5];
            $tariffa = $arr[6];
            $ev = $arr[7];
            $prodotti = "";
            $user = $modal->getCurrentUserId($_SESSION['user']);

            $aaa = $modal->isWalkable($anno, $mese, $giorno, $ora, $campo);
            if ($aaa) {
                if ($modal->checkReservation($anno, $mese, $giorno, $ora, $campo)) {
                    die("<div class='modal-content'> <h4> Spazio occupato </h4> </div>");
                } else {
                    $content = "<div class='modal-content'>"
                            . "<h5 class='center'>" . $modal->getCampoNome($campo) . "</h5>"
                            . $modal->getNiceDate($anno, $mese, $giorno, $h, $m)
                            . "<h5 class='center'>" . $modal->setPrenota($anno, $mese, $giorno, $ora, $campo, $prodotti, $ev, $tariffa, $user) . "</h5>"
                            . $modal->getFooterEvento($value)
                            . "</div>";
                    echo $content;
                }
            } else {
                die("Spazio Occupato");
            }
        }
    } else {
        die("BadRequest");
    }
} else {
    die("BadRequest");
}


