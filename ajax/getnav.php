<?php
session_start();
if (isset($_SESSION['user'])) {

    include '../class/pannelloadmin.php';
    $admin = new Amministratore();
    ?>
    <div class='navbar-fixed'>
        <nav>
            <div class="nav-wrapper grey">
                <a data-activates="mobilenav" class="button-collapse waves-effect waves-light" id="sidenav"><i class="material-icons">menu</i></a>
                <ul class="brand-logo" id="navigazione">
                    <li><a class="btn-floating btn-flat waves-effect waves-light btnindietro"><i class="material-icons">fast_rewind</i></a></li>
                    <li id='datacalendario'><?php echo date('d/m/Y') ?></li>
                    <li id='title'></li>
                    <li><a class="btn-floating btn-flat waves-effect waves-light btnavanti"><i class="material-icons">fast_forward</i></a></li>
                </ul>
                <ul class="right hide-on-med-only hide-on-small-only">
                    <li><a class="dropdown-button" data-activates="campidropdown" >NomeCampo123<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a id="calendario">Calendario</a></li>
                    <?php
                    if ($admin->getPrivileges()) {
                        ?>
                        <li><a id="campi">Campi</a></li>
                        <li><a id="tariffe">Tariffe</a></li>
                        <li><a id="extra">Prodotti</a></li>
                        <li><a id="utenti">Utenti</a></li>
                        <!--<li><a id="attivita">Attività</a></li>
                        <li><a id="generali">Generali</a></li>
                        <li><a id="prodotti">Prodotti</a></li>
                        <li><a id="prenotazioni">Prenotazioni</a></li>
                        <li><a id="analisi">Analisi</a></li>
                        <li><a id="utenti">Utenti</a></li>-->
                        <?php
                    }
                    ?>
                    <li><a id="usersession">Logout</a></li>
                </ul>
                <ul class="side-nav" id="mobilenav">
                    <li><div class="userView">
                            <div class="background">
                                <img src="images/campo.jpg">
                            </div>
                            <a href="#!user"><img class="circle" src="images/user.png"></a>
                            <a href="#!name"><span class="white-text name">Tennis</span></a>
                        </div></li>
                    <li><a id="calendario">Calendario</a></li>
                    <?php
                    if ($admin->getPrivileges()) {
                        ?>
                        <li><a id="campi">Campi</a></li>
                        <li><a id="tariffe">Tariffe</a></li>
                        <li><a id="extra">Prodotti</a></li>
                        <li><a id="utenti">Utenti</a></li>
                        <!--<li><a id="attivita">Attività</a></li>
                        <li><a id="generali">Generali</a></li>
                        <li><a id="prodotti">Prodotti</a></li>
                        <li><a id="prenotazioni">Prenotazioni</a></li>
                        <li><a id="analisi">Analisi</a></li>
                        <li><a id="utenti">Utenti</a></li>-->
                        <?php
                    }
                    ?>
                    <li><a id="usersession">Logout</a></li>
                </ul>
            </div>
        </nav>

    </div>
    <ul id="campidropdown" class='dropdown-content'>
        <?php
        echo $admin->getCampi();
        ?>
    </ul>
    <?php
} else {
    die("BadRequest");
}
?>



