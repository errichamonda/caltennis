<?php

session_start();
if (isset($_REQUEST['val']) && !empty($_SESSION['user'])) {
    include '../class/modalprenota.php';
    $modal = new ModalPrenota();
    $value = $_REQUEST['val'];

    $arr = explode("/", $value);
    $anno = $arr[0];
    $mese = $arr[1];
    $giorno = $arr[2];
    $h = $arr[3];
    $m = $arr[4];
    $ora = $arr[3] . "/" . $arr[4];
    $campo = $arr[5];

    $modal->setCampo($campo);
    //$modal->setTariffa("$mese/$giorno");

    if ($modal->isWalkable($anno, $mese, $giorno, $ora, $campo)) {
        if (!$modal->checkReservation($anno, $mese, $giorno, $ora, $campo)) {
            die("<div class='modal-content'> <h4> Spazio occupato </h4> </div>");
        } else {//if ($modal->canSub()) {
            if ($modal->isEvento($anno, $mese, $giorno, $ora, $campo)) {
                $content = "<div class='modal-content'>"
                        . "<h5 class='center'>" . $modal->getCampoName() . "</h5>"
                        . "<p class='center'>" . $modal->getNomeAttivita() . "</p>"
                        . $modal->getNiceDate($anno, $mese, $giorno, $h, $m)
                        . "<p class='flow-text center green-text' id='loadph'> Puoi prenotare un posto su questa attività </p>"
                        . "<p class='center' id='prezzomodal'>" . $modal->getPrezzo() . "</p></div>"                        
                        . $modal->getFooterEvento($value)
                        . "</div>";
                echo $content;
            } else {
                $content = "<div class='modal-content'>"
                        . "<h5 class='center'>" . $modal->getCampoName() . "</h5>"
                        . $modal->getNiceDate($anno, $mese, $giorno, $h, $m)
                        . "<p class='flow-text center green-text' id='loadph'> Questo campo è ancora libero </p>"
                        //. "<p class='center' id='prezzomodal'>" . $modal->getTabPrezzo() . "</p>"
                        . "<div class='row'>";
                if ($modal->getIntProdotti() > 0) {
                    $content .= "<div class='col s6'>" . $modal->getTabPrezzo() . "</div>"
                            . "<div class='col s6'>" . $modal->getProdotti() . "</div>";
                } else {
                    $content .= "<div class='col s6 offset-s3 center'>" . $modal->getTabPrezzo() . "</div>";
                }
                $content .= "</div>"
                        . $modal->getFooter($value)
                        . "</div>";
                echo $content;
            }
        } /*else {
            die("Non hai permessi");
        }*/
    } else {
        die("Errore");
    }
} else {
    die("Bad request");
}

