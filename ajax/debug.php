<!DOCTYPE html>
<html>
    <head>
        <!--Import Google Icon Font-->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="../css/cal.css" media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="../css/easy-autocomplete.min.css" media="screen,projection"/>

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    </head>
    <body>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="../js/jquery.easy-autocomplete.min.js"></script>
        <script type="text/javascript" src="../js/materialize.min.js"></script>
        <script>
            $(function () {
                var option = {
                    url: "utentijson.php",
                    getValue: "nome",
                    list: {
                        onSelectItemEvent: function () {
                            var value = $('#test123').getSelectedItemData().id;
                            $('#test123').attr("value", value);
                        },
                        match: {
                            enabled: true
                        }
                    }
                };
                $("#test123").easyAutocomplete(option);
            });

            $(document).on('click', ".btn", function () {
                console.log($("#test123").attr('value'));
            });


        </script>
        <input id="test123" />

        <a class="btn">Test</a>


        <?php
        include '../class/modalprenota.php';
        $test = new ModalPrenota();
        $test->setCampo(2);
        print_r($test->getCampo());
        print_r($test->getAutoFill());
        ?>
    </body>
</html>    

