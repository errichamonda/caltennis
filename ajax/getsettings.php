<?php

session_start();
if (!empty($_SESSION['user'])) {
    include '../class/setting.php';
    $setting = new Setting();
    if ($setting->getPrivileges()) {
        if ($_REQUEST['what'] == "campi") {
            $setting->setCampi();
            echo $setting->getCampi();
        }
        if ($_REQUEST['what'] == "calendariocampi") {
            echo $setting->calendarioCampi($_REQUEST['id'], $_REQUEST['periodo']);
        }
        if ($_REQUEST['what'] == "tariffe") {
            echo $setting->getTariffe();
        }
        if ($_REQUEST['what'] == "prodotti") {
            echo $setting->getProdotti();
        }
        if ($_REQUEST['what'] == "newprodottomodal") {
            echo $setting->modalNewProdotto();
        }
        if ($_REQUEST['what'] == "newprodotto") {
            if ($setting->newProdottoDuplicate($_REQUEST['nome'])) {
                die("Nome duplicato");
            } else {
                echo $setting->newProdotto($_REQUEST['nome']);
            }
        }
        if ($_REQUEST['what'] == "modificatariffe") {
            try {
                echo $setting->modificaTariffe($_REQUEST['id'], $_REQUEST['arr']);
            } catch (Exception $ex) {
                die("Errore " . $ex);
            }
        }
        if ($_REQUEST['what'] == "newtariffamodal") {
            echo $setting->modalNewTariffa();
        }
        if ($_REQUEST['what'] == "newtariffa") {
            try {
                print_r($setting->newTariffa($_REQUEST['nome'], $_REQUEST['sigla']));
            } catch (Exception $ex) {
                die("Errore " . $ex);
            }
        }
        if ($_REQUEST['what'] == "modificaextra") {
            try {
                echo $setting->modificaExtra($_REQUEST['id'], $_REQUEST['arr']);
            } catch (Exception $ex) {
                die("Errore " . $ex);
            }
        }
        if ($_REQUEST['what'] == "newperiodomodal") {
            echo $setting->modalNewPeriodo($_REQUEST['campo']);
        }
        if ($_REQUEST['what'] == "newperiodo") {
            echo $setting->newPeriodo($_REQUEST['campo'], $_REQUEST['data']);
        }
        if ($_REQUEST['what'] == "utenti") {
            echo $setting->utentiPage();
        }
        if ($_REQUEST['what'] == "newsociomodal") {
            echo $setting->newSocioModal();
        }
        if ($_REQUEST['what'] == "newsocio") {
            echo $setting->newSocio($_REQUEST['socio']);
        }
        if ($_REQUEST['what'] == "modificatariffa") {
            echo $setting->saveCalendarioTariffa();
        }
        if ($_REQUEST['what'] == "eliminatariffe") {
            echo $setting->eliminaTariffa($_REQUEST['campo'], $_REQUEST['id'], $_REQUEST['periodo']);
        }
    }
    /*
      if ($_REQUEST['what'] == "generali") {
      echo $setting->getGeneral();
      }
      if ($_REQUEST['what'] == "prodotti") {
      echo $setting->getProdotti();
      }
      if ($_REQUEST['what'] == "prenotazioni") {
      echo $setting->getPrenotazioni();
      }
      if ($_REQUEST['what'] == "modalprodotti") {
      $setting->setCampi();
      $setting->setProdotti();
      echo $setting->modalProdotti($_REQUEST['id']);
      }
      if ($_REQUEST['what'] == "soci") {
      echo $setting->getSoci();
      }
      if ($_REQUEST['what'] == "modalsoci") {
      $setting->setCampi();
      $setting->setSoci();
      echo $setting->modalChiprenota($_REQUEST['id']);
      }
      if ($_REQUEST['what'] == "newcampo") {
      echo $setting->newCampoModal();
      }
      if ($_REQUEST['what'] == "newextra") {
      echo $setting->newExtraModal();
      }
      if ($_REQUEST['what'] == "modalattivita") {
      $setting->setCampi();
      $setting->setAttivita();
      echo $setting->modalAttivita($_REQUEST['id']);
      }
      if ($_REQUEST['what'] == "attivita"){
      $setting->setAttivita();
      echo $setting->getAttivita();
      }
      if ($_REQUEST['what'] == "newattivita") {
      echo $setting->getModalAttivita();
      }
      if ($_REQUEST['what'] == "modalattsub") {
      $setting->setAttivita();
      $setting->setSoci();
      echo $setting->getModalAttivitaSub($_REQUEST['id']);
      }
      if ($_REQUEST['what'] == "modalgiorni") {
      echo $setting->getModalGiorni($_REQUEST['id']);
      } */
} else {
    echo "errore";
}
