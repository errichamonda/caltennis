<?php

session_start();
if (!empty($_SESSION['user'])) {
    include '../class/setting.php';
    $setting = new Setting();
    if ($setting->getPrivileges()) {
        if (isset($_REQUEST['cosa'])) {
            if ($_REQUEST['cosa'] == "campi") {
                $id = $_REQUEST['id'];
                $nome = $_REQUEST['nome'];
                $valore = $_REQUEST['valore'];
                $orarioIn = $_REQUEST['orarioIn'] . ":00";
                $orarioFi = $_REQUEST['orarioFi'] . ":00";
                $prezzo = $_REQUEST['prezzo'];
                $extra = $_REQUEST['extra'];
                $soci = $_REQUEST['soci'];
                $att = $_REQUEST['att'];
                $closing = $_REQUEST['closing'];
                $query = "UPDATE campo SET stato = '$valore', cnome = '$nome', cprezzo = '" . $prezzo * 100 . "', orainizio = '$orarioIn'"
                        . " , orafine = '$orarioFi', oid = '$extra', tid = '$soci', attivita = '$att', chiusure = '$closing' WHERE cid = $id";
                $res = wrap_db_query($query);

                if ($res) {
                    die("Eseguito");
                } else {
                    die("fail");
                }
            } 
            elseif ($_REQUEST['cosa'] == "extra") {
                $id = $_REQUEST['id'];
                $nome = $_REQUEST['nome'];
                $stato = $_REQUEST['valore'];
                $prezzo = $_REQUEST['prezzo'] * 100;
                $query = "UPDATE opzioni SET onome = '$nome', ostato = '$stato', oprezzo = '$prezzo' WHERE oid= $id";
                $res = wrap_db_query($query);
                if ($res) {
                    die("Eseguito");
                } else {
                    die("fail");
                }
            }
            elseif ($_REQUEST['cosa'] == "modificaattivita"){
                $id = $_REQUEST['id'];
                $nome = $_REQUEST['nome'];
                $maxu = $_REQUEST['maxutenti'];
                $cancreate = $_REQUEST['cancreate'];
                $cansub = $_REQUEST['cansub'];
                $query = "UPDATE attivita SET anome = '$nome', amaxutenti = '$maxu', acreator = '$cancreate', asub = '$cansub' WHERE aid = $id";
                
                $res = wrap_db_query($query);
                if ($res) {
                    die("Eseguito");
                } else {
                    die("fail");
                }
            }
            elseif ($_REQUEST['cosa'] == "newcampo") {
                $nome = $_REQUEST['nome'];
                $prezzo = $_REQUEST['prezzo'] * 100;
                $orainizio = $_REQUEST['apertura'] . ":00";
                $orafine = $_REQUEST['chiusura'] . ":00";
                $query = "INSERT INTO campo (`cnome`, `cprezzo`, `orainizio`, `orafine`, `oid`, `tid`) VALUES ('$nome', '$prezzo', '$orainizio', '$orafine', '','0')";
                $res = wrap_db_query($query);
                if($res){
                    die("Eseguito");
                } else {
                    die("fail");
                }
            }
            elseif ($_REQUEST['cosa'] == "newextra"){
                $nome = $_REQUEST['nome'];
                $prezzo = $_REQUEST['prezzo'] * 100;
                $query = "INSERT INTO opzioni (`onome`, `ostato`, `oprezzo`) VALUES ('$nome', '0', '$prezzo')";
                $res = wrap_db_query($query);
                if($res){
                    die("Eseguito");
                } else {
                    die("fail");
                }
            }
            elseif ($_REQUEST['cosa'] == "newsocio") {
                $nome = $_REQUEST['nome'];
                $query = "INSERT INTO tipologia (`tnome`) VALUES ('$nome')";
                $res = wrap_db_query($query);
                if($res){
                    die("Eseguito");
                } else {
                    die("fail");
                }
            }
            elseif ($_REQUEST['cosa'] == "newattivita"){
                $nome = $_REQUEST['nome'];
                $maxutenti = $_REQUEST['maxutenti'];
                $db_query = "INSERT INTO attivita (`anome`, `amaxutenti`, `acreator`, `asub`) VALUES ('$nome', $maxutenti, '0', '0')";
                $res = wrap_db_query($db_query);
                if($res){
                    die("Eseguito");
                } else {
                    die("fail");
                }
            }
            else {
                die("Errore inserimento");
            }
        } else {
            die("Errore dati");
        }
    } else {
        die("Errore");
    }
} else {
    die("Non permesso");
}
