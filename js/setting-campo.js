$(document).on('click', '#campi', function () {
    window.location.hash = "#campi";
});

function loadSettingCampi() {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "campi"
        },
        success: function (result) {
            if (result != "errore") {
                $('#divtabella').html(result);
                $('.btnindietro').hide();
                $('.btnavanti').hide();
                $('#datacalendario').hide();
                $('select').material_select();
                $('.collapsible').collapsible();
                $('#title').html("&nbsp;Campo");
                $('.dropdown-button').hide();
                $('.tooltipped').tooltip({delay: 50});
                /*$('.tablecampi > tbody > tr').each(function () {
                 $('.timepicki').timepicki({
                 start_time: [getTimepickiTime($(this))],
                 show_meridian: false,
                 min_hour_value: 0,
                 max_hour_value: 23,
                 step_size_minutes: 30,
                 overflow_minutes: true,
                 increase_direction: 'up',
                 disable_keyboard_mobile: true
                 });
                 $('.timepickf').timepicki({
                 start_time: [getTimepickiTime($(this))],
                 show_meridian: false,
                 min_hour_value: 0,
                 max_hour_value: 23,
                 step_size_minutes: 30,
                 overflow_minutes: true,
                 increase_direction: 'up',
                 disable_keyboard_mobile: true
                 });
                 });*/

                $(".setted").click(function () {
                    if ($(this).attr("contentEditable") == true) {
                        $(this).attr("contentEditable", "false");
                    } else {
                        $(this).attr("contentEditable", "true");
                    }
                });
            } else {
                Materialize.toast("Non permesso", 4000);
                window.location.hash = "#home";

            }
        }
    });
}

$(document).on('click', '.addperiodo', function () {

    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "newperiodomodal",
            campo: $(this).attr('value')
        },
        success: function (result) {
            $('#modalprenota').html(result);
            $('#modalprenota').modal("open");
            $('.datepicker').pickadate({
                selectMonth: true,
                selectYears: false,
                format: 'dd/mm',
                container: '.container'
            });
        }
    })
});

$(document).on('click', '.calendariotariffe', function () {
    var id = $(this).closest('table').attr('value');
    var periodo = $(this).closest('tr').children('td:first').text();
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "calendariocampi",
            id: id,
            periodo: periodo
        },
        success: function (result) {
            window.location.hash = "#modcal";
            $('#divtabella').html(result);
            var isMouseDown = false,
                    isHighlighted;
            $("#campotable .tar")
                    .mousedown(function (event) {
                        switch (event.which) {
                            case 1:
                            {
                                var colore = $('input[name=radiobuttontariffe]:checked').attr('colore');
                                isMouseDown = true;

                                $(this).attr("class", "tar " + colore);
                                $(this).html($('input[name=radiobuttontariffe]:checked').attr('value'));
                                $(this).attr("vaid", $('input[name=radiobuttontariffe]:checked').attr('vaid'));
                                if ($(this).length > 0) {
                                    isHighlighted = true;
                                } else {
                                    isHighlighted = false;
                                }
                                return false; // prevent text selection
                                break;
                            }

                        }

                    })
                    .mouseover(function () {
                        if (isMouseDown) {
                            var colore = $('input[name=radiobuttontariffe]:checked').attr('colore');
                            $(this).attr("class", "tar " + colore);
                            $(this).html($('input[name=radiobuttontariffe]:checked').attr('value'));
                            $(this).attr("vaid", $('input[name=radiobuttontariffe]:checked').attr('vaid'));
                        }
                    })
                    .bind("selectstart", function () {
                        return false;
                    })

            $(document).mouseup(function () {
                isMouseDown = false;
            });

        }
    })
});

$(document).on('click', '.calendariotariffesalva', function () {
    Materialize.toast("salva", 4000);
    $('#modalprenota').modal("close");
});

$(document).on('click', '#newperiodo', function () {
    if ($('#datanewperiodo').val() != "") {
        $.ajax({
            url: "ajax/getsettings.php",
            type: "POST",
            data: {
                what: "newperiodo",
                data: $('#datanewperiodo').val(),
                campo: $(this).attr('value')
            },
            success: function (result) {
                Materialize.toast("Eseguito", 4000);
                $('#modalprenota').modal('close');
                loadSettingCampi();
            }
        });
    } else {
        Materialize.toast("Data errata", 4000);
    }
    /**/
});

$(document).on('click', '.coloretariffa', function () {
    var colore = $(this).attr('value');
    var btn = $(this);
    var id = $(this).attr('idtariffa');
    $.ajax({
        url: "ajax/color.php",
        type: "POST",
        data: {
            colore: colore,
            tariffa: id
        },
        success: function (result) {
            btn.removeClass(colore);
            btn.addClass(result);
            btn.attr('value', result);
        }
    })
});

$(document).on('click', '#savecalendariotariffa', function () {
    var campo = $(this).attr('campo');
    var periodo = $(this).attr('periodo');
    console.log("Campo " + campo + " Periodo " + periodo);

    var gucciboi = true;
    var array = [];
    $("#campotable tbody tr").each(function () {
        var add = [];
        var primo = true;
        $('td', this).each(function () {
            if (primo) {
                add.push($(this).html().trim());
                primo = false;
            } else {
                if (isNaN($(this).attr('vaid')) && ($(this).html().trim() !== "")) {
                    Materialize.toast("Errore valori", 4000);
                    $(this).css("background-color", "red");
                    gucciboi = false;
                }
                add.push($(this).attr("vaid"));
            }
        });
        array.push(add);
    });
    if (gucciboi) {
        $.ajax({
            url: "ajax/getsettings.php",
            type: "POST",
            data: {
                what: "modificatariffa",
                campo: campo,
                periodo: periodo,
                array: array
            },
            success: function (result) {
                Materalize.toast("Eseguito", 4000);
                //Materialize.toast(result, 4000);
            }
        })
    }
});

$(document).on('click', '.eliminacalendariotariffe', function () {
   var id = $(this).closest('tr').attr('value');
   var campo = $(this).closest('table').attr('value');
   var periodo = $(this).closest('tr').find('td').eq(0).html();
   $.ajax({
       url: "ajax/getsettings.php",
       type: "POST",
       data: {
           what: "eliminatariffe",
           id: id,
           periodo: periodo,
           campo: campo
       },
       success: function (result) {
           Materialize.toast(result, 4000);
           loadSettingCampi();
       }
   })
});