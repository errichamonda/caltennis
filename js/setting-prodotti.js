$(document).on('click', '#extra', function () {
    window.location.hash = "#prodotti";
});

function loadSettingProdotti() {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "prodotti"
        },
        success: function (result) {
            if (result != "errore") {
                $('#divtabella').html(result);
                $('.btnindietro').hide();
                $('.btnavanti').hide();
                $('#datacalendario').hide();
                $('.collapsible').collapsible();
                $('#title').html("&nbsp;Prodotti");
                $('.dropdown-button').hide();
                $('.tooltipped').tooltip({delay: 50});
                $(".setted").click(function () {
                    if ($(this).attr("contentEditable") == true) {
                        $(this).attr("contentEditable", "false");
                    } else {
                        $(this).attr("contentEditable", "true");
                    }
                });
            } else {
                Materialize.toast("Non permesso", 4000);
                window.location.hash = "#home";

            }
        }
    });
}

$(document).on('click', '#newprodotto', function () {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "newprodottomodal"
        },
        success: function (result) {
            $('#modalprenota').html(result);
            $('#modalprenota').modal('open');
        }
    });
});

$(document).on('click', '#confermanuovoprodotto', function () {
    var nome = $("#nomenewextra").val();
    var check = nome.replace(/\s/g, "");
    if (check == "") {
        Materialize.toast("Nome non valido", 4000);
    } else {
        $.ajax({
            url: "ajax/getsettings.php",
            type: "POST",
            data: {
                what: "newprodotto",
                nome: nome
            },
            success: function (result) {
                if (result === "end") {
                    $('#modalprenota').modal("close");
                    $('#modalprenota').html("ciao");
                    window.location.hash = "#prodotti";
                } else {
                    Materialize.toast(result, 4000);
                }
            }
        });
    }

});


$(document).on('click', '.saveextratab', function () {
    var gucciboi = true;
    var value = $(this).attr('value');
    var idtabella = "#extra" + value;
    var array = [];
    $(idtabella + " thead tr").each(function () {
        var a = [];
        $('th', this).each(function () {
            a.push($(this).attr('value'));
        });
        array.push(a);
    });
    $(idtabella + " tbody tr").each(function () {
        var add = [];
        var primo = true;
        $('td', this).each(function () {
            $(this).css("background-color", "");
            if (primo) {
                if (isNaN($(this).attr('value'))) {
                    Materialize.toast("Errore NaN", 4000);
                    gucciboi = false;
                }
                add.push($(this).attr('value'));
                primo = false;
            } else {
                if (isNaN($(this).attr('value')) || isNaN($(this).html())) {
                    Materialize.toast("Errore valori", 4000);
                    $(this).css("background-color", "red");
                    gucciboi = false;
                }
                add.push($(this).html());
            }
        });
        array.push(add);
    });
    if (gucciboi) {
        $.ajax({
            url: "ajax/getsettings.php",
            type: "POST",
            data: {
                what: "modificaextra",
                id: value,
                arr: array
            },
            success: function (result) {
                if (result == "") {
                    Materialize.toast("Eseguito", 4000);
                } else {
                    Materialize.toast("Errore");
                }
            }
        });
    }
});

