function loadModal(sValue) {
    $.ajax({
        url: "./ajax/prenota.php",
        type: "POST",
        data: {
            val: sValue
        },
        success: function (result) {
            if (result.trim() == "Errore") {
                Materialize.toast("Errore", 4000);
            } else {
                $("#modalprenota").html(result);
                $("input:radio[name=attivita]:first").attr('checked', true);
                $("#modalprenota").modal("open");
            }
        }
    });

    window.location.hash = "#home";
}

function inviaPrenotazioneEvento(sValue) {
    $.ajax({
        url: "./ajax/inviaprenotazione.php",
        type: "POST",
        data: {
            val: sValue,
            what: "ev"
        },
        success: function (result) {
            $("#modalprenota").html(result);
            $("#btnmodprenota").text("chiudi");
            $("#btnmodprenota").attr("id", "btnmodchiudi");
            loadCalendario();
        }
    });
}

$(document).on('click', "#btnmodprenota", function () {
    var user = $('.chip').map(function () {
        return $(this).attr('value');
    }).get().join(',');
    var value = $(this).attr('value');
    $.ajax({
        url: "./ajax/inviaprenotazione.php",
        type: "POST",
        data: {
            val: value,
            utenti: user,
            what: "noev"
        },
        success: function (result) {
            $("#modalprenota").html(result);
            $("#btnmodprenota").text("chiudi");
            $("#btnmodprenota").attr("id", "btnmodchiudi");
            loadCalendario();
        }
    });
});

$(document).on('click', "#btnmodprenotaev", function () {
    inviaPrenotazioneEvento($(this).attr('value') + "/ ");
});

$(document).on('click', '.btnprenota', function () {
    loadModal($(this).attr('value'));
});


$(document).on('click', '#btnmodnext', function () {
    var prodotti = $('.prodotto:checked').map(function () {
        return this.value;
    }).get().join(',');

    var tipo = $("input:radio[name=attivita]:checked").attr('value');

    if (prodotti == "") {
        var value = $(this).attr('value') + "/" + tipo + "/ ";
    } else {
        var value = $(this).attr('value') + "/" + tipo + "/" + prodotti;
    }
    //console.log(value);
    $.ajax({
        url: 'ajax/riassuntoprenotazione.php',
        type: 'POST',
        data: {
            what: "fill",
            val: value
        },
        success: function (result) {
            //console.log(result);
            $('#modalprenota').html(result);
            $('#user_autocomplete').autoComplete({
                source: function (term, response) {
                    $.getJSON('ajax/utentijson.php', {q: term, val: value}, function (data) {
                        response(data);
                    });
                },
                renderItem: function (item, search) {
                    // escape special characters
                    search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                    return '<div class="autocomplete-suggestion" data-id="' + item[0] + '" data-val="' + item[1] + '">' + item[1].replace(re, "<b>$1</b>") + '</div>';
                },
                onSelect: function (e, term, item) {
                    $('#user_id').val(item.data('id'));
                    var aaa = $('#utentiplaceholder').html();
                    aaa += "<div class='chip' value='" + item.data('id') + "'>" + item.data('val') + "<i class='close material-icons'>close</i></div>";
                    $('#utentiplaceholder').html(aaa);
                    $('#user_autocomplete').val();
                }
            });
        }
    });
});

$(document).on('click', '#btnmodfill', function () {
    var user = $('.chip').map(function () {
        return $(this).attr('value');
    }).get().join(',');
    var value = $(this).attr('value');
    $.ajax({
        url: "ajax/riassuntoprenotazione.php",
        type: 'POST',
        data: {
            what: "riassunto",
            val: value,
            utenti: user
        },
        success: function (result) {
            $('#modalprenota').html(result);
        }
    });
});

$(document).on('click', '#eliminapreno', function () {
    var value = $(this).attr('value');
    $.ajax({
        url: "ajax/lookup.php",
        type: 'POST',
        data: {
            what: "elimina",
            val: value
        },
        success: function (result) {
            $('#modalprenota').modal('close');
            Materialize.toast("Eliminato", 4000);
            $('#calendario').click();
        }
    });
});

$(document).on('click', '.addguest', function () {
    var aaa = $('#utentiplaceholder').html();
    aaa += "<div class='chip' value='0'> Guest <i class='close material-icons'>close</i></div>";
    $('#utentiplaceholder').html(aaa);
});

