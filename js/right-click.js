/*$(document).on("contextmenu", ".btnprenota", function (event) {
 
 // Avoid the real one
 event.preventDefault();
 
 var aggiungi = "<li id='prenotame'>Prenota per me</li><li id='prenotaaltri'>Prenota per altri</li>";
 $('.custom-menu').html(aggiungi);
 // Show contextmenu
 $(".custom-menu").finish().toggle(100).
 // In the right position (the mouse)
 css({
 top: event.pageY + "px",
 left: event.pageX + "px"
 });
 });*/

$(document).on("contextmenu", ".occupato", function (event) {
    // Avoid the real one
    event.preventDefault();
    var aggiungi = "<li id='lookpreno'>Chi ha prenotato</li><li id='annullapreno'>Annulla prenotazione</li>";
    $('.custom-menu').html(aggiungi);

    $('#lookpreno').attr('value', $(this).attr('value'));
    $('#annullapreno').attr('value', $(this).attr('value'));
    // Show contextmenu
    $(".custom-menu").finish().toggle(100).
            // In the right position (the mouse)
            css({
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
});

$(document).on("click", "#annullapreno", function () {
    var value = $(this).attr('value');
    $.ajax({
        url: "ajax/lookup.php",
        type: 'POST',
        data: {
            what: "delete",
            val: value
        },
        success: function (result) {
            $('.custom-menu').hide();
            $('#modalprenota').html(result);
            $('#modalprenota').modal("open");
        }
    });
});

$(document).on('click', '#lookpreno', function () {
    var value = $(this).attr('value');
    $.ajax({
        url: "ajax/lookup.php",
        type: 'POST',
        data: {
            what: "lookup",
            val: value
        },
        success: function (result) {
            $('.custom-menu').hide();
            $('#modalprenota').html(result);
            $('#modalprenota').modal("open");
        }
    });
});