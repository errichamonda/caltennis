$(document).on('click', '#tariffe', function () {
    window.location.hash = "#tariffe";
});

function loadSettingTariffe() {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "tariffe"
        },
        success: function (result) {
            if (result != "errore") {
                $('#divtabella').html(result);
                $('.btnindietro').hide();
                $('.btnavanti').hide();
                $('#datacalendario').hide();
                $('.collapsible').collapsible();
                $('#title').html("&nbsp;Tariffe");
                $('.dropdown-button').hide();
                $('.tooltipped').tooltip({delay: 50});
                $(".setted").click(function () {
                    if ($(this).attr("contentEditable") == true) {
                        $(this).attr("contentEditable", "false");
                    } else {
                        $(this).attr("contentEditable", "true");
                    }
                });
            } else {
                Materialize.toast("Non permesso", 4000);
                window.location.hash = "#home";

            }
        }
    });
}

$(document).on('click', '.savetariffatab', function () {
    var gucciboi = true;
    var value = $(this).attr('value');
    var idtabella = "#tariffa" + value;
    var array = [];
    $(idtabella + " thead tr").each(function () {
        var a = [];
        $('th', this).each(function () {
            a.push($(this).attr('value'));
        });
        array.push(a);
    });
    $(idtabella + " tbody tr").each(function () {
        var add = [];
        var primo = true;
        $('td', this).each(function () {
            $(this).css("background-color", "");
            if (primo) {
                if (isNaN($(this).attr('value'))) {
                    Materialize.toast("Errore NaN", 4000);
                    gucciboi = false;
                }
                add.push($(this).attr('value'));
                primo = false;
            } else {
                if (isNaN($(this).attr('value')) || isNaN($(this).html())) {
                    Materialize.toast("Errore valori", 4000);
                    $(this).css("background-color", "red");
                    gucciboi = false;
                }
                add.push($(this).html());
            }
        });
        array.push(add);
    });
    if (gucciboi) {
        $.ajax({
            url: "ajax/getsettings.php",
            type: "POST",
            data: {
                what: "modificatariffe",
                id: value,
                arr: array
            },
            success: function (result) {
                console.log(result);
                if (result == "") {
                    Materialize.toast("Eseguito", 4000);
                } else {
                    Materialize.toast("Errore");
                }
            }
        });
    }
});

$(document).on('click', '#newtariffamodal', function () {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "newtariffamodal"
        },
        success: function (result) {
            $('#modalprenota').html(result);
            $('#modalprenota').modal("open");
            $('input#shortnametariffa').characterCounter();
        }
    });
});

$(document).on('click', '#newtariffa', function () {
    var nome = $('#nomenewtariffe').val();
    var sigla = $('#shortnametariffa').val();
    var check = nome.replace(/\s/g, "");
    if (sigla.length != 3 || check == "") {
        Materialize.toast("Lunghezza sigla", 4000);
    } else {
        $.ajax({
            url: "ajax/getsettings.php",
            type: "POST",
            data: {
                what: "newtariffa",
                nome: nome,
                sigla: sigla
            },
            success: function (result) {
                if(result == "Duplicato") {
                    Materialize.toast(result, 4000);
                } else {
                    loadSettingTariffe();
                    $('#modalprenota').modal('close');
                }
            }
        });
    }
});