function loadSettingsUtenti() {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "utenti"
        },
        success: function (result) {
            if (result != "errore") {
                $('#divtabella').html(result);
                $('.btnindietro').hide();
                $('.btnavanti').hide();
                $('#datacalendario').hide();
                $('#title').html("&nbsp;Utenti");
                $('.dropdown-button').hide();
                $('ul.tabs').tabs();
                $('#utentitabella').DataTable({
                    scrollY: 380,
                    paging: false
                });
                $('#prenotazionitab').DataTable({
                    scrollY: 380,
                    paging: false
                });
            } else {
                Materialize.toast("Non permesso", 4000);
                window.location.hash = "#home";
            }
        }
    });
}

$(document).on('click', '#newsocio', function () {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "newsociomodal"
        },
        success: function (result) {
            $('#modalprenota').html(result);
            $('#modalprenota').modal('open');
        }
    });
});

$(document).on('click', '#btnnewsocio', function () {
    var nome = $('#nomenewsocio').val();
    if (nome.trim() == "") {
        Materialize.toast("Nome non valido", 4000);
    } else {
        $.ajax({
            url: "ajax/getsettings.php",
            type: "POST",
            data: {
                what: "newsocio",
                socio: nome
            },
            success: function (result) {
                console.log(result);
                $('#modalprenota').modal('close');
                $('#soci').click();
            }
        });
    }
});


$(document).on('click', '#utenti', function () {
    window.location.hash = "#utenti";
});