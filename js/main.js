$(function () {
    window.location.hash = '#!';
});

$.getScript("js/prenotazione.js");
$.getScript("js/setting-campo.js");
$.getScript("js/setting-tariffe.js");
$.getScript("js/setting-prodotti.js");
$.getScript("js/setting-utenti.js");
$.getScript("js/right-click.js");

window.onhashchange = function () {
    $('#title').html("");
    pageId = window.location.hash;
    $.ajax({
        url: "ajax/getuser.php",
        type: "POST",
        success: function (result) {
            if (result) {
                if (pageId === '#!') {
                    $.ajax({
                        url: "ajax/getpage.html",
                        type: "POST",
                        success: function (result) {
                            $(".conte").html(result);
                            loadNav();
                            $('.modal').modal();
                        }
                    });
                }
                if (pageId === '#home') {
                    loadCalendario();
                }
                if (pageId === '#login') {
                    history.back();
                    Materialize.toast("Già loggato", 4000);
                }
                if (pageId === '#prenotamodal') {
                    window.location.hash = "#home";
                }
                if (pageId === "#campi") {
                    loadSettingCampi();
                }
                if (pageId === "#tariffe") {
                    loadSettingTariffe();
                }
                if (pageId === "#prodotti") {
                    loadSettingProdotti();
                }
                if (pageId === "#utenti") {
                    loadSettingsUtenti();
                }
                if (pageId === "#modcal") {
                }
            } else {
                loadLogin();
            }
        }
    });
}

function loadLogin() {
    window.location.hash = "#login";
    $.ajax({
        url: "ajax/access/login.html",
        type: "POST",
        success: function (result) {
            $('.conte').html(result);
        }
    });
}

function getToday() {
    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '-' +
            (month < 10 ? '0' : '') + month + '-' +
            (day < 10 ? '0' : '') + day;

    return output;
}

function createDate(sData) {
    var res = sData.split("-");
    return res[2] + "/" + res[1] + "/" + res[0];
}

function getDataFromValue() {
    var sValue = $("#datacalendario").text();
    var res = sValue.split("/");
    return res[2] + "-" + res[1] + "-" + res[0];
}

function aggiornaData(sDate, iAddDays) {
    var d = new Date(sDate);
    d.setDate(d.getDate() + parseInt(iAddDays));
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = d.getFullYear() + '-' +
            (month < 10 ? '0' : '') + month + '-' +
            (day < 10 ? '0' : '') + day;
    return output;
}

function loadCalendario() {
    window.location.hash = "#home";
    var ismobile = false;
    var sData = getDataFromValue();
    /*if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
     ismobile = true;
     }*/
    $('.btnavanti').attr('value', aggiornaData(sData, 1));
    $('.btnindietro').attr('value', aggiornaData(sData, -1));
    $.ajax({
        url: "./ajax/tabella.php",
        type: "POST",
        data: {
            date: sData,
            mobile: ismobile,
            campo: $(".selezionato").attr('value')
        },
        success: function (result) {
            $("#divtabella").html(result);
            $('.btnindietro').show();
            $('.btnavanti').show();
            $('#datacalendario').show();
            $('.dropdown-button').show();
        }
    });

}

function loadNav() {
    $.ajax({
        url: "./ajax/getnav.php",
        type: "POST",
        success: function (result) {
            $('#navi').html(result);
            window.location.hash = "#home";
            $("#sidenav").sideNav({
                closeOnClick: true
            });
            $(".dropdown-button").dropdown();
            $(".dropdown-button").html($('.selezionato').html() + '<i class="material-icons right">arrow_drop_down</i>');
        }
    });
}
/*
 function loadSettings() {
 $.ajax({
 url: "ajax/getsettings.php",
 type: "POST",
 success: function (result) {
 if (result != "errore") {
 $('#divtabella').html(result);
 $('.btnindietro').hide();
 $('.btnavanti').hide();
 $('#datacalendario').hide();
 $('select').material_select();
 $('.collapsible').collapsible();
 $('.tablecampi > tbody > tr').each(function () {
 $('.timepicki').timepicki({
 start_time: [getTimepickiTime($(this))],
 show_meridian: false,
 min_hour_value: 0,
 max_hour_value: 23,
 step_size_minutes: 30,
 overflow_minutes: true,
 increase_direction: 'up',
 disable_keyboard_mobile: true
 });
 $('.timepickf').timepicki({
 start_time: [getTimepickiTime($(this))],
 show_meridian: false,
 min_hour_value: 0,
 max_hour_value: 23,
 step_size_minutes: 30,
 overflow_minutes: true,
 increase_direction: 'up',
 disable_keyboard_mobile: true
 });
 });
 } else {
 Materialize.toast("Non permesso", 4000);
 window.location.hash = "#home";
 
 }
 }
 });
 }
 */
function getTimepickiTime(obj) {
    var TimeValue = obj.val();
    var trimmedTimeValue = TimeValue.replace(/ /g, '');
    var resultSplit = trimmedTimeValue.split(':');
    return resultSplit;
}
/*
function loadTabPrenotazioni(sData) {
    $.ajax({
        url: "ajax/prenotazionitabella.php",
        type: "POST",
        data: {
            val: sData
        },
        success: function (result) {
            $('#prenotatabella').html(result);
            $('#prenotazionitab').DataTable({
                scrollY: 380,
                paging: false
            });
        }
    })
}*/

function loadProdotti() {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "prodotti"
        },
        success: function (result) {
            if (result != "errore") {
                $('#divtabella').html(result);
                $('.btnindietro').hide();
                $('.btnavanti').hide();
                $('#datacalendario').hide();
                $('select').material_select();
                $(".setted").click(function () {
                    if ($(this).attr("contentEditable") == true) {
                        $(this).attr("contentEditable", "false");
                    } else {
                        $(this).attr("contentEditable", "true");
                    }
                });
            } else {
                Materialize.toast("Non permesso", 4000);
                window.location.hash = "#home";
            }
        }
    });
}

$(document).on('click', '#login', function () {
    var un = $("#username").val();
    var pw = $("#password").val();
    $.ajax({
        url: "ajax/access/login_check.php",
        type: "POST",
        data: {
            username: un,
            password: pw
        },
        success: function (result) {
            if (result.trim() === "allow") {
                window.location.hash = "#!";
            } else {
                Materialize.toast(result, 4000);
            }
        }
    });
});

$(document).on('click', '#usersession', function () {
    $.ajax({
        url: "ajax/access/logout.php",
        type: "POST",
        success: function (result) {
            if (result === "Logged out") {
                window.location.hash = "#login";
                Materialize.toast("Logout", 4000);
            } else {
                $('.calendario').html(result);
            }
        }
    });
});

$(document).on('click', '#btnmodchiudi', function () {
    $("#modalprenota").modal("close");
});

$(document).on('click', '.btnindietro', function () {
    $('#datacalendario').html(createDate($(this).attr('value')));
    loadCalendario();
});

$(document).on('click', '.btnavanti', function () {
    $('#datacalendario').html(createDate($(this).attr('value')));
    loadCalendario();
});

$(document).on('click', '.settbtn', function () {
    var id = $(this).attr("value");
    var nome = $(this).closest('tr').find('td').eq(0).text();
    var valore = $(this).closest('tr').find('select').val();
    var orarioIn = $(this).closest('tr').find('td').eq(2).find('input').eq(0).attr('data-timepicki-tim') + ":" + $(this).closest('tr').find('td').eq(2).find('input').eq(0).attr('data-timepicki-mini');
    var orarioFi = $(this).closest('tr').find('td').eq(3).find('input').eq(0).attr('data-timepicki-tim') + ":" + $(this).closest('tr').find('td').eq(3).find('input').eq(0).attr('data-timepicki-mini');
    var prezzo = $(this).closest('tr').find('td').eq(4).text();
    var extra = $(this).closest('tr').find('td').eq(5).find('a').attr('value');
    var soci = $(this).closest('tr').find('td').eq(6).find('a').attr('value');
    var att = $(this).closest('tr').find('td').eq(7).find('a').attr('value');
    //console.log(id + " " + nome + " " + valore + " " + orarioIn + " " + orarioFi + " " + prezzo + " " + id + " " + extra);  
    $.ajax({
        url: "ajax/modifiche.php",
        type: "POST",
        data: {
            id: id,
            nome: nome,
            valore: valore,
            orarioIn: orarioIn,
            orarioFi: orarioFi,
            prezzo: prezzo,
            extra: extra,
            soci: soci,
            att: att,
            cosa: "campi"
        },
        success: function (result) {
            Materialize.toast(result, 4000);
        }
    });
});
/*
$(document).on('click', '.prenotatab', function () {
    loadTabPrenotazioni($(this).attr("id"));
});*/

$(document).on('click', '#generali', function () {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "generali"
        },
        success: function (result) {
            if (result != "errore") {
                $('#divtabella').html(result);
                $('.btnindietro').hide();
                $('.btnavanti').hide();
                $('#datacalendario').hide();
            } else {
                Materialize.toast("Non permesso", 4000);
                window.location.hash = "#home";
            }
        }
    });
});

$(document).on('click', '#prodotti', function () {
    loadProdotti();
});
/*
$(document).on('click', '#prenotazioni', function () {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "prenotazioni"
        },
        success: function (result) {
            if (result != "errore") {
                $('#divtabella').html(result);
                $('.btnindietro').hide();
                $('.btnavanti').hide();
                $('#datacalendario').hide();
            } else {
                Materialize.toast("Non permesso", 4000);
                window.location.hash = "#home";
            }
        }
    });
});*/

$(document).on('click', '#soci', function () {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "soci"
        },
        success: function (result) {
            if (result != "errore") {
                $('#divtabella').html(result);
                $('.btnindietro').hide();
                $('.btnavanti').hide();
                $('#datacalendario').hide();
                $(".setted").click(function () {
                    if ($(this).attr("contentEditable") == true) {
                        $(this).attr("contentEditable", "false");
                    } else {
                        $(this).attr("contentEditable", "true");
                    }
                });
            } else {
                Materialize.toast("Non permesso", 4000);
                window.location.hash = "#home";
            }
        }
    });
});


$(document).on('click', '.settprod', function () {
    var id = $(this).closest('tr').attr('value');
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "modalprodotti",
            id: id
        },
        success: function (result) {
            $('#modalprenota').html(result);
            $('#modalprenota').modal('open');
            $('#modalprenota').attr('value', id);
        }
    });
});

$(document).on('click', '.settsoci', function () {
    var id = $(this).closest('tr').attr('value');
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "modalsoci",
            id: id
        },
        success: function (result) {
            $('#modalprenota').html(result);
            $('#modalprenota').modal('open');
            $('#modalprenota').attr('value', id);
        }
    });
});

$(document).on('click', '#btnmodextra', function () {
    var prodotti = $('.prodotto:checked').map(function () {
        return this.value;
    }).get().join(',');
    $id = $('#modalprenota').attr('value');
    //console.log($("tr[value='" + $id + "']").find('td').eq(5).find('.settprod').text());
    $("tr[value='" + $id + "']").find('td').eq(5).find('.settprod').attr('value', prodotti);
    $('#modalprenota').modal('close');
});

$(document).on('click', '.extramod', function () {
    $id = $(this).attr("value");
    $nome = $(this).closest('tr').find('td').eq(0).text();
    $valore = $(this).closest('tr').find('select').val();
    $prezzo = $(this).closest('tr').find('td').eq(2).text();
    $.ajax({
        url: "ajax/modifiche.php",
        type: "POST",
        data: {
            cosa: "extra",
            id: $id,
            nome: $nome,
            valore: $valore,
            prezzo: $prezzo
        },
        success: function (result) {
            Materialize.toast(result, 4000);
        }
    });
});

$(document).on('click', '#btnmodsoci', function () {
    var prodotti = $('.socimod:checked').map(function () {
        return this.value;
    }).get().join(',');
    $id = $('#modalprenota').attr('value');
    //console.log($("tr[value='" + $id + "']").find('td').eq(5).find('.settprod').text());
    $("tr[value='" + $id + "']").find('td').eq(6).find('.settsoci').attr('value', prodotti);
    $('#modalprenota').modal('close');
});

$(document).on('click', '#newcampo', function () {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "newcampo"
        },
        success: function (result) {
            $('#modalprenota').html(result);
            $('#modalprenota').modal('open');
            $('#inizionewcampo').timepicki({
                start_time: ["06", "00"],
                show_meridian: false,
                min_hour_value: 0,
                max_hour_value: 23,
                step_size_minutes: 30,
                overflow_minutes: true,
                increase_direction: 'up',
                disable_keyboard_mobile: true
            });
            $('#finenewcampo').timepicki({
                start_time: ["18", "00"],
                show_meridian: false,
                min_hour_value: 0,
                max_hour_value: 23,
                step_size_minutes: 30,
                overflow_minutes: true,
                increase_direction: 'up',
                disable_keyboard_mobile: true
            });
        }
    });
});

$(document).on('click', '#btnnewcampo', function () {
    var nome = $('#nomenewcampo').val();
    var prezzo = $('#prezzonewcampo').val();
    var apertura = $('#inizionewcampo').val();
    if (apertura.length == 4) {
        apertura += "0";
    }
    var chiusura = $('#finenewcampo').val();
    if (chiusura.length == 4) {
        chiusura += "0";
    }
    if (nome === "" || prezzo === "" || apertura === "" || chiusura === "") {
        Materialize.toast("Errore nei dati", 4000);
    } else {
        $.ajax({
            url: "ajax/modifiche.php",
            type: "POST",
            data: {
                cosa: "newcampo",
                nome: nome,
                prezzo: prezzo,
                apertura: apertura,
                chiusura: chiusura
            },
            success: function (result) {
                Materialize.toast(result, 4000);
            }
        });
        $('#modalprenota').modal('close');
        loadCampi();
    }
});
/*
 $(document).on('click', '#newextra', function () {
 $.ajax({
 url: "ajax/getsettings.php",
 type: "POST",
 data: {
 what: "newextra"
 },
 success: function (result) {
 $('#modalprenota').html(result);
 $('#modalprenota').modal('open');
 }
 });
 });
 
 $(document).on('click', '#btnnewextra', function () {
 var nome = $('#nomenewextra').val();
 var prezzo = $('#prezzonewextra').val();
 $.ajax({
 url: "ajax/modifiche.php",
 type: "POST",
 data: {
 cosa: "newextra",
 nome: nome,
 prezzo: prezzo
 },
 success: function (result) {
 Materialize.toast(result, 4000);
 $('#modalprenota').modal('close');
 loadProdotti();
 }
 });
 });*/




$(document).on('click', '#analisi', function () {
    $.ajax({
        url: "ajax/analisi.php",
        type: "POST",
        data: {
            what: "analisi"
        },
        success: function (result) {
            $('#divtabella').html(result);
            $('.btnindietro').hide();
            $('.btnavanti').hide();
            $('#datacalendario').hide();
            $('.datepicker').pickadate({
                selectMonths: true,
                selectYears: 100,
                format: 'dd-mm-yyyy'
            });
        }
    });
});

$(document).on('click', '.settatt', function () {
    var id = $(this).closest('tr').attr('value');
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "modalattivita",
            id: id
        },
        success: function (result) {
            $('#modalprenota').html(result);
            $('#modalprenota').modal('open');
            $('#modalprenota').attr('value', id);
        }
    });
});

$(document).on('click', '#btnmodatt', function () {
    var attivita = $('.attimod:checked').map(function () {
        return this.value;
    }).get().join(',');
    if (attivita === "") {
        Materialize.toast("Selezione non valida", 4000);
    } else {
        $id = $('#modalprenota').attr('value');
        $("tr[value='" + $id + "']").find('td').eq(7).find('.settatt').attr('value', attivita);
        $('#modalprenota').modal('close');
    }
});

$(document).on('click', '#attivita', function () {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "attivita"
        },
        success: function (result) {
            if (result != "errore") {
                $('#divtabella').html(result);
                $('.btnindietro').hide();
                $('.btnavanti').hide();
                $('#datacalendario').hide();
            } else {
                Materialize.toast("Non permesso", 4000);
                window.location.hash = "#home";
            }
        }
    });
});

$(document).on('click', '#btnnewattivita', function () {
    var nome = $('#nomenewattivita').val();
    var maxutenti = $('#maxutentiattivita').val();
    $.ajax({
        url: "ajax/modifiche.php",
        type: "POST",
        data: {
            cosa: "newattivita",
            nome: nome,
            maxutenti: maxutenti
        },
        success: function (result) {
            Materialize.toast(result, 4000);
            $('#modalprenota').modal('close');
            $('#attivita').click();
        }
    });
});

$(document).on('click', '#newattivita', function () {
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "newattivita"
        },
        success: function (result) {
            $('#modalprenota').html(result);
            $('#modalprenota').modal('open');
        }
    });
});

$(document).on('click', '.attcreator', function () {
    Materialize.toast("Da aggiungere", 4000);
});

$(document).on('click', '.attsub', function () {
    var id = $(this).closest('tr').attr('value');
    $.ajax({
        url: "ajax/getsettings.php",
        type: "POST",
        data: {
            what: "modalattsub",
            id: id
        },
        success: function (result) {
            $('#modalprenota').html(result);
            $('#modalprenota').modal('open');
            $('#modalprenota').attr('value', id);
        }
    });
});

$(document).on('click', '.settgiorni', function () {
    var id = $(this).closest('tr').attr('value');
    $.ajax({
        url: 'ajax/getsettings.php',
        type: 'POST',
        data: {
            what: 'modalgiorni',
            id: id
        },
        success: function (result) {
            $('#modalprenota').html(result);
            $('#modalprenota').modal('open');
            $('#modalprenota').attr('value', id);
        }
    })
});

$(document).on('click', '#btnmodattsub', function () {
    var prodotti = $('.socimod:checked').map(function () {
        return this.value;
    }).get().join(',');
    $id = $('#modalprenota').attr('value');
    //console.log($("tr[value='" + $id + "']").find('td').eq(5).find('.settprod').text());
    $("tr[value='" + $id + "']").find('td').eq(3).find('.attsub').attr('value', prodotti);
    $('#modalprenota').modal('close');
});

$(document).on('click', '.modificaattivita', function () {
    var id = $(this).closest('tr').attr('value');
    var nome = $(this).closest('tr').find('td').eq(0).text().trim();
    var maxutenti = $(this).closest('tr').find('td').eq(1).text();
    var cancreate = $(this).closest('tr').find('td').eq(2).find('a').attr('value');
    var cansub = $(this).closest('tr').find('td').eq(3).find('a').attr('value');
    $.ajax({
        url: 'ajax/modifiche.php',
        type: 'POST',
        data: {
            cosa: "modificaattivita",
            id: id,
            nome: nome,
            maxutenti: maxutenti,
            cancreate: cancreate,
            cansub: cansub
        },
        success: function (result) {
            Materialize.toast(result, 4000);
            $('#attivita').click();
        }
    });
});

$(document).on('click', '#calendario', function () {
    window.location.hash = "#home";
});


$(document).on('click', "#campidropdown a", function () {
   $(".selezionato").attr("class", "");
   $(this).attr("class", "selezionato");
   $(".dropdown-button").html($('.selezionato').html() + '<i class="material-icons right">arrow_drop_down</i>');
   loadCalendario();
});