<?php

class Database{
    
    private $conn;
    
    public function __construct() {
        include 'declares.php';
    }
    
    private function Connect(){
        try{
            $this->conn = new PDO("mysql:host=".global_mysql_server.";dbname=".global_mysql_database, global_mysql_user, global_mysql_password);
            $this->conn->setAttribute(PDO__ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $ex) {
            die("Errore db");
        }
    }
    
    public function doQuery($db_query){
        $this->Connect();
        $stmt = $this->conn->prepare($db_query);
        $stmt->execute();
    }
}


